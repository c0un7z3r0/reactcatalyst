/* eslint-disable */
import zipFolder from 'zip-folder';
import packageJson from '../package.json';
import path from 'path';
import fs from 'fs';

let versionNum = packageJson.version;
let projectName = packageJson.name;

//Zip /dist/ folder contents to a file called ReactCatalyst-vX.X.X.zip, this is temporarily placed in the root folder...
zipFolder(path.resolve(__dirname,'../dist/'), path.resolve(__dirname,'../' + projectName + '-v' + versionNum + '.zip'), function(err) {
  if(err) {
    console.log('There was a problem packaging ' + projectName + '-v' + versionNum + '.zip. ERROR: '+err, err);
  } else {
//..we then move the newly created zip from the root into the /dist/ folder.
// (Doing it this way prevents an empty zip file appearing in the created zip file - chicken and egg error)
    fs.renameSync(path.resolve(__dirname,'../' + projectName + '-v' + versionNum + '.zip'),path.resolve(__dirname,'../dist/' + projectName + '-v' + versionNum + '.zip'));
    //Log Success!
    console.log('Zipped up ' + projectName + ' v' + versionNum + ' Successfully. The exported zip file has been placed in your /dist/ folder');
  }
});
