import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../src/store/configureStore';
const store = configureStore();

import {
  Router,
  Route,
  Link
} from 'react-router-dom';

import {createBrowserHistory} from 'history'
const history = createBrowserHistory()

const AllTheProviders = ({ children }) => {
  console.log("CUSTOM RENDERED")
  return (
    <Provider store={store}>
      <Router history={history}>
        {children}
      </Router>
    </Provider>
  )
}

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options })

function renderWithRouter(
  ui,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {}
) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history,
  }
}

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender, renderWithRouter }