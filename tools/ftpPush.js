/* eslint-disable */

import FtpDeployModule from 'ftp-deploy';
import ftpConfig from '../envConfig.js';

let ftpDeploy= new FtpDeployModule();

console.log('Deploying to ' + ftpConfig.host + ' ...');
console.log(' ');
console.log('...Please wait...');

ftpDeploy.deploy(ftpConfig, function(err) {
  if (err) console.log('ERROR RETURNED: ' + err);
  else console.log('... all files successfully uploaded to '+ ftpConfig.host + '. Deployment Complete');
});
