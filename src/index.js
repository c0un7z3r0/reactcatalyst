import React from 'react';
import { render } from 'react-dom';
import 'babel-polyfill';
import App from './app.jsx';

render(
  <App />,
  document.getElementById('app')
);
