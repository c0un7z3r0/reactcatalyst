import React from 'react';
import PropTypes from 'prop-types';

import NavMenu from './navMenu/navMenu.jsx'

//import styles from './mainLayout.scss';

class NavContainer extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {

  let NavContents = [
    {label:"Home",          path: "/",              icon: "ion-ios-home" , key:"0" },
    {label:"Documentation", path: "/Documentation", icon: "ion-ios-book" , key:"1" },
    {label:"Components",    path: "/Components",    icon: "ion-ios-flask" , key:"2" },
    {label:"Dependencies",  path: "/Dependencies",  icon: "ion-nuclear" , key:"3" },
  ];
  //{label:"Examples",      path: "/Examples",      icon: "ion-cube" , key:"4" },

    return(
      <NavMenu items={NavContents} />
    )
  }
}

NavContainer.propTypes = {
};

export default NavContainer;