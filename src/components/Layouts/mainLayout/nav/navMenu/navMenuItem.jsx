import React from 'react';
import PropTypes from 'prop-types';

import Linky from '../../../../common/linky/linky.jsx';

import styles from './navMenu.scss';

class NavMenuItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <li className={styles.navMenuItem}>
        <Linky  to={this.props.path}
          exact
          className={styles.navLink}
          activeClassName={styles.navLinkActive}
        > 

          <span className={styles.navIcon+" "+this.props.icon}></span>
          
          <span className={styles.navLabel}>
            {this.props.label}
          </span>

        </Linky>
      </li>
    )
  }
}

NavMenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  icon: PropTypes.string
};

export default NavMenuItem;