import React from 'react';
import PropTypes from 'prop-types';

import NavMenuItem from './navMenuItem.jsx'

import styles from './navMenu.scss';

class NavMenu extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {

    let MenuItemList = this.props.items.map((item) =>
      <NavMenuItem 
        label={item.label}
        path={item.path}
        icon={item.icon}
        key={item.key}
      />
    )

    return(
      <ul className={styles.navMenu}>
        {MenuItemList}
      </ul>
    )
  }
}

NavMenu.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
      icon: PropTypes.string,
      key: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired
};

export default NavMenu;