import React from 'react';
import PropTypes from 'prop-types';
// import Header from '../header/header.jsx';
import { connect } from 'react-redux';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import NavMenu from './nav/navMenuContainer.jsx';

import HomePage from '../../Pages/homePage/homePage.jsx';
import Documentation from '../../Pages/documentationPage/documentationPage.jsx';
import Dependencies from '../../Pages/dependenciesPage/dependenciesPage.jsx';
import Components from '../../Pages/componentsPage/componentsPage.jsx';

import styles from './mainLayout.scss';

import icon from './ReactCatalystLogo_v2.svg';
import fireEscape from './fire_escape.svg';

class mainLayout extends React.Component {

  render() {
    return(
      <Router>
        <div className={styles.container}>
          <div className={styles.iconContainer}>
            <img src={icon} />
          </div>
          <header>
            <img src={fireEscape} />
            <h1><strong>REACT CATALYST <sup>v.{CATALYST.VERSION}</sup></strong></h1>
          </header>
          <nav>
            <NavMenu />
          </nav>
          <main>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/Documentation" component={Documentation} />
            <Route exact path="/Components" component={Components} />
            <Route exact path="/Dependencies" component={Dependencies} />
            {/* 
            <Route exact path="/Examples" component={Examples} /> 
            */}
          </main>
        </div>
      </Router>
    );
  }
}

mainLayout.propTypes = {
  //children: PropTypes.object.isRequired,
  //loading: PropTypes.bool.isRequired
  //routes: PropTypes.array.isRequired
};

export default mainLayout;
