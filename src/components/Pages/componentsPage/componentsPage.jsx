import React from 'react';
import PropTypes from 'prop-types';
import Code from '../../common/code/code.jsx';

import TextInput from '../../common/forms/textInput/textInput.jsx';
import SelectInput from '../../common/forms/selectInput/selectInput.jsx';
import TextArea from '../../common/forms/textArea/textArea.jsx';
import SubmitInput from '../../common/forms/submitInput/submitInput.jsx';
import Button from '../../common/Button/button.jsx';
import SwitchCheckBox from '../../common/forms/switchCheckBox/switchCheckBox.jsx';

import styles from './componentsPage.scss';

const dudes = [
  {
    value: 'a',
    text: 'Iron Man'
  },
  {
    value: 'b',
    text: 'Hulk'
  },
  {
    value: 'c',
    text: 'Thor'
  }
];

const formData = {
  checkSwitch: false
};


class ComponentsPage extends React.Component {
  constructor(props) {
    super(props);
  }

  emptyFunction(){
    //blank function, checkSwitch needs something to bind to at the mo.
  }

  render() {
    return(
      <div className={styles.componentsPageContainer}>
        <div className={styles.asideSection}>
          <h1><span className="ion-ios-flask"></span><strong>Components</strong></h1> 
          <ol className={styles.articleMenu}>
            <li>TextArea</li>
            <li>TextInput</li>
            <li>SelectInput</li>
            <li>SubmitInput</li>
            <li>SwitchCheckbox</li>
            <li>Button</li>
          </ol>
        </div>
        <div className={styles.contentSection}>

          <h1><strong>Reusable Components</strong></h1>
          <h4>Themed Webpage Components Styled for a consistent look.</h4>

          <hr />

          <h3>Form Elements</h3>

          <h5>TextArea</h5>
          <div className="row">
            <div className="col5">
              <h6>Preview</h6>
              <div>
                <TextArea
                  //error="Error Message"
                  value="Default Text Content"
                  rows="8"
                  label="TextArea Component"
                  maxlength="100"
                  name="badman"
                  placeholder="This is a placeholder"
                  wrap="soft"
                />
              </div>
              <h6>Description</h6>
              <div>
                <p>The TextArea component is a standard <strong>Textarea</strong> HTML5 component augmented with additional features such as error messages, highlighting, custom styles, user defined labels and more.</p>
              </div>
            </div>
            <div className="col7">
              <h6>Usage (JSX)</h6>
              <Code>
                &lt;TextArea<br/>
                &nbsp;&nbsp;value="Default Text Content"<br/>
                &nbsp;&nbsp;rows="8"<br/>
                &nbsp;&nbsp;label="TextArea Component"<br/>
                &nbsp;&nbsp;maxlength="100"<br/>
                &nbsp;&nbsp;name="formName"<br/>
                &nbsp;&nbsp;placeholder="This is a placeholder"<br/>
                &nbsp;&nbsp;wrap="soft"<br/>
                /&gt;
              </Code>
            </div>
          </div>
          <div className="row">
            <h6>Props</h6>
            <div className="col12">
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Default</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>autoFocus</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Preselects focus to this element on page load if this is set to true.</td>
                  </tr>
                  <tr>
                    <td>classname</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the textarea element.</td>
                  </tr>
                  <tr>
                    <td>cols</td>
                    <td>number</td>
                    <td>auto</td>
                    <td>Sets the width of the element specifying how many horizontal characters spaces the textarea element should have. If not set the element will automatically resize to fit the width of the container.</td>
                  </tr>
                  <tr>
                    <td>defaultValue</td>
                    <td>string</td>
                    <td></td>
                    <td>String of text to be used as the default text. If not set a blank textarea will be rendered.</td>
                  </tr>
                  <tr>
                    <td>disabled</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Creates a read-only textarea.</td>
                  </tr>
                  <tr>
                    <td>error</td>
                    <td>string</td>
                    <td></td>
                    <td>When a string is set on error the string is used as an error message to feed back to the user why the form has failed validation.</td>
                  </tr>
                  <tr>
                    <td>form</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies one or more forms the text area belongs to.</td>
                  </tr>
                  <tr>
                    <td>label</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears in the label above the form element.</td>
                  </tr>
                  <tr>
                    <td>maxLength</td>
                    <td>number</td>
                    <td></td>
                    <td>Sets the maximum number of characters the textarea can accommodate.</td>
                  </tr>
                  <tr>
                    <td>name</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a name for the text area.</td>
                  </tr>
                  <tr>
                    <td>onChange</td>
                    <td>func</td>
                    <td></td>
                    <td>Function to be called when any changes are made to the content of the textarea.</td>
                  </tr>
                  <tr>
                    <td>placeholder</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies placeholder text. This is text that appears in the textarea as a label, the default behaviour for a placeholder is that it should disappear when the textarea has focus.</td>
                  </tr>
                  <tr>
                    <td>readonly</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies that a text area should be read-only.</td>
                  </tr>
                  <tr>
                    <td>required</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Specifies that a text area must be filled out.</td>
                  </tr>
                  <tr>
                    <td>rows</td>
                    <td>number</td>
                    <td>1</td>
                    <td>Sets the height of the element specifying how many veritical characters spaces the textarea element should have. </td>
                  </tr>
                  <tr>
                    <td>value</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears pre-entered in the form at render.</td>
                  </tr>
                  <tr>
                    <td>wrap</td>
                    <td>string</td>
                    <td>hard</td>
                    <td>Set to either 'hard' or 'soft', this property determines whether the text should wrap to the word or to the letter, respectively. </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <hr />

          <h5>TextInput</h5>
          <div className="row">
            <div className="col5">
              <h6>Preview</h6>
              <div>

                <TextInput
                  name="textInputExample"
                  label="Text Input Label"
                  value="Text Input"
                  placeholder="Placeholder Text"
                  onChange={this.emptyFunction}
                  classname="myClassName" />

              </div>
              <h6>Description</h6>
              <div>
                <p>TextInput is a standard HTML5 Form <strong>Input</strong> component that has additional features such as error messages, labels and more.</p>
              </div>
            </div>
            <div className="col7">
              <h6>Usage (JSX)</h6>

              <Code>
                &lt;TextInput<br/>
                &nbsp;&nbsp;name=&quot;textInputExample&quot;<br/>
                &nbsp;&nbsp;label=&quot;Text Input Label&quot;<br/>
                &nbsp;&nbsp;value=&quot;Text Input&quot;<br/>
                &nbsp;&nbsp;placeholder=&quot;Placeholder Text&quot;<br/>
                &nbsp;&nbsp;onChange=&#123;this.emptyFunction&#125;<br/>
                &nbsp;&nbsp;classname=&quot;myClassName&quot;<br/>
                /&gt;
              </Code>

            </div>
          </div>
          <div className="row">
            <h6>Props</h6>
            <div className="col12">
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Default</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>autoComplete</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Enable autocomplete when set to true.</td>
                  </tr>
                  <tr>
                    <td>autoFocus</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Preselects focus to this element on page load if this is set to true.</td>
                  </tr>
                  <tr>
                    <td>className</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the textarea element.</td>
                  </tr>
                  <tr>
                    <td>defaultValue</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies preset value of formfield when set.</td>
                  </tr>
                  <tr>
                    <td>disabled</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies whether the input element should be disabled.</td>
                  </tr>
                  <tr>
                    <td>error</td>
                    <td>string</td>
                    <td></td>
                    <td>When a string is set on this error property the string is used as an error message to feed back to the user why the form has failed validation.</td>
                  </tr>
                  <tr>
                    <td>form</td>
                    <td>string</td>
                    <td></td>
                    <td>Used to specify the form or forms that the input element belongs to.</td>
                  </tr>
                  <tr>
                    <td>maxLength</td>
                    <td>num</td>
                    <td></td>
                    <td>Specifies the number of characters allowed in this input element.</td>
                  </tr>
                  <tr>
                    <td>name</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a name for the form input element.
                    </td>
                  </tr>
                  <tr>
                    <td>onChange</td>
                    <td>func</td>
                    <td></td>
                    <td>Function to be called when any changes are made to the content of the textarea.</td>
                  </tr>
                  <tr>
                    <td>pattern</td>
                    <td>regexp</td>
                    <td></td>
                    <td>Specifies a regular expression that the input element's value is checked against.</td>
                  </tr>
                  <tr>
                    <td>placeholder</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies placeholder text. This is text that appears in the form input element as a description, the default behaviour for a placeholder is that it should disappear when the textarea has focus.</td>
                  </tr>
                  <tr>
                    <td>readOnly</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies that a form input element should be read-only.
                    </td>
                  </tr>
                  <tr>
                    <td>required</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies that a text area must be filled out.</td>
                  </tr>
                  <tr>
                    <td>size</td>
                    <td>num</td>
                    <td></td>
                    <td>Sets the width of the element specifying how many horizontal characters spaces the form input element should have. If not set the element will automatically resize to fit the width of the container.</td>
                  </tr>
                  <tr>
                    <td>value</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears pre-entered in the form at render.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <hr />

          <h5>SelectInput</h5>
          <div className="row">
            <div className="col5">
              <h6>Preview</h6>
              <div>

                <SelectInput
                  name="authorId"
                  label="SelectInput"
                  defaultOption="Select Dropdown"
                  options={dudes}
                  classname="TESTCLASS" />

              </div>
              <h6>Description</h6>
              <div>
                <p>SelectInput is a standard HTML5 Form <strong>Select</strong> component that has additional features such as error messages, labels and more.</p>
              </div>
            </div>
            <div className="col7">
              <h6>Usage (JSX)</h6>

              <Code>
                const optionHeroes = &#91;<br/>
                &nbsp;&nbsp;&#123;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;value: 'a',<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;text: 'Iron Man'<br/>
                &nbsp;&nbsp;&#125;,<br/>
                &nbsp;&nbsp;&#123;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;value: 'b',<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;text: 'Hulk'<br/>
                &nbsp;&nbsp;&#125;,<br/>
                &nbsp;&nbsp;&#123;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;value: 'c',<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;text: 'Thor'<br/>
                &nbsp;&nbsp;&#125;<br/>
                &#93;;<br/>
                &nbsp;<br/>
                &lt;SelectInput<br/>
                &nbsp;&nbsp;name=&quot;authorId&quot;<br/>
                &nbsp;&nbsp;label=&quot;SelectInput&quot;<br/>
                &nbsp;&nbsp;defaultOption=&quot;Select Dropdown&quot;<br/>
                &nbsp;&nbsp;options=&#123;optionHeroes&#125;<br/>
                &nbsp;&nbsp;classname=&quot;TESTCLASS&quot; <br/>
                /&gt;
              </Code>

            </div>
          </div>
          <div className="row">
            <h6>Props</h6>
            <div className="col12">
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Default</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>autoFocus</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Preselects focus to this element on page load if this is set to true.</td>
                  </tr>
                  <tr>
                    <td>classname</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the select dropdown element.</td>
                  </tr>
                  <tr>
                    <td>defaultOption</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a initial option without a value which can be used as a placeholder or instruction 'select option' for example.
                    </td>
                  </tr>
                  <tr>
                    <td>disabled</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies whether the input element should be disabled.</td>
                  </tr>
                  <tr>
                    <td>error</td>
                    <td>string</td>
                    <td></td>
                    <td>When a string is set on this error property the string is used as an error message to feed back to the user why selected option has failed validation.</td>
                  </tr>
                  <tr>
                    <td>form</td>
                    <td>string</td>
                    <td></td>
                    <td>Used to specify the form or forms that the select option element belongs to.
                    </td>
                  </tr>
                  <tr>
                    <td>label</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears in the label above the select input element.</td>
                  </tr>
                  <tr>
                    <td>multiple</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies that multiple options can be selected at once.</td>
                  </tr>
                  <tr>
                    <td>name</td>
                    <td>string</td>
                    <td></td>
                    <td>Defines a name for the select dropdown list</td>
                  </tr>
                  <tr>
                    <td>onChange</td>
                    <td>func</td>
                    <td></td>
                    <td>Function to be called when a new selection is made from the select dropdown.
                    </td>
                  </tr>
                  <tr>
                    <td>options</td>
                    <td>array (Object)</td>
                    <td></td>
                    <td>Array of options for select dropdown menu. The first string in the object should be the value, the second the text to be used to represent this selection. </td>
                  </tr>
                  <tr>
                    <td>required</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies that a select element option must be selected to pass validation.
                    </td>
                  </tr>
                  <tr>
                    <td>size</td>
                    <td>number</td>
                    <td></td>
                    <td>Sets the width of the element specifying how many horizontal characters spaces the form input element should have. If not set the element will automatically resize to fit the width of the container.
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <hr />

          <h5>SubmitInput</h5>
          <div className="row">
            <div className="col5">
              <h6>Preview</h6>
              <div>

                <SubmitInput
                  name="submitInputName"
                  label="SelectInputLabel"
                  value="GO"
                  classname="myClassName" />

              </div>
              <h6>Description</h6>
              <div>
                <p><strong>SubmitInput</strong> is a Form Submit button component enhanced with error message, label and validation features built in.</p>
              </div>
            </div>
            <div className="col7">
              <h6>Usage (JSX)</h6>

              <Code>
                &lt;SubmitInput<br/>
                &nbsp;&nbsp;name=&quot;submitInputName&quot;<br/>
                &nbsp;&nbsp;label=&quot;SelectInputLabel&quot;<br/>
                &nbsp;&nbsp;value=&quot;GO&quot;<br/>
                &nbsp;&nbsp;classname=&quot;myClassName&quot;<br/>
                /&gt;
              </Code>

            </div>
          </div>
          <div className="row">
            <h6>Props</h6>
            <div className="col12">
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Default</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>autoFocus</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Preselects focus to this element on page load if this is set to true.</td>
                  </tr>
                  <tr>
                    <td>disabled</td>
                    <td>bool</td>
                    <td></td>
                    <td>Creates a read-only form submit button.</td>
                  </tr>
                  <tr>
                    <td>form</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies one or more forms the form submit button belongs to.</td>
                  </tr>
                  <tr>
                    <td>formAction</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies the URL of the file that will process the input control when the form is submitted.</td>
                  </tr>
                  <tr>
                    <td>formEncType</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies how the form-data should be encoded when submitting it to the server</td>
                  </tr>
                  <tr>
                    <td>formMethod</td>
                    <td></td>
                    <td></td>
                    <td>Defines the HTTP method for sending data to the action URL (GET or POST)</td>
                  </tr>
                  <tr>
                    <td>formNoValidate</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies that form elements should not be validated when submitted</td>
                  </tr>
                  <tr>
                    <td>formTarget</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies where to display the response that is received after submitting the form</td>
                  </tr>
                  <tr>
                    <td>name</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a name for the submit input button.</td>
                  </tr>
                  <tr>
                    <td>value</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies the value of the select input button.</td>
                  </tr>
                  <tr>
                    <td>label</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears in the label above the select input button.</td>
                  </tr>
                  <tr>
                    <td>onClick</td>
                    <td>string</td>
                    <td></td>
                    <td>Function to be called when the form submit button is clicked.</td>
                  </tr>
                  <tr>
                    <td>classname</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the form submit button.</td>
                  </tr>
                  <tr>
                    <td>error</td>
                    <td>string</td>
                    <td></td>
                    <td>When a string is set on error the string is used as an error message to feed back to the user why the form has failed validation.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>


          <hr />

          <h5>Switch CheckBox</h5>
          <div className="row">
            <div className="col5">
              <h6>Preview</h6>
              <div>

                <SwitchCheckBox
                  label="Switch Label"
                  onChange={this.emptyFunction()}
                  defaultChecked={formData.checkSwitch}
                  checked={formData.checkSwitch}
                  value=""
                />

              </div>
              <h6>Description</h6>
              <div>
                <p>CheckSwitchBox is a standard HTML5 checkbox styled to have the appearance of a toggle switch.</p>
              </div>
            </div>
            <div className="col7">
              <h6>Usage (JSX)</h6>

              <Code>
                &lt;SwitchCheckBox<br/>
                &nbsp;&nbsp;label=&quot;Switch Label&quot;<br/>
                &nbsp;&nbsp;onChange=&#123;this.emptyFunction()&#125;<br/>
                &nbsp;&nbsp;defaultChecked=&#123;formData.checkSwitch&#125;<br/>
                &nbsp;&nbsp;checked=&#123;formData.checkSwitch&#123;<br/>
                &nbsp;&nbsp;value=&quot;&quot;<br/>
                /&gt;
              </Code>

            </div>
          </div>
          <div className="row">
            <h6>Props</h6>
            <div className="col12">
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Default</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>autoFocus</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Preselects focus to this element on page load if this is set to true.</td>
                  </tr>
                  <tr>
                    <td>checked</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies whether the Switch Checkbox is checked.</td>
                  </tr>
                  <tr>
                    <td>defaultChecked</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies whether the Switch Checkbox should be checked by default at render.</td>
                  </tr>
                  <tr>
                    <td>defaultValue</td>
                    <td>string</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>disabled</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies whether the Select Checkbox element should be disabled.</td>
                  </tr>
                  <tr>
                    <td>form</td>
                    <td>string</td>
                    <td></td>
                    <td>Used to specify the form or forms that the select option element belongs to.</td>
                  </tr>
                  <tr>
                    <td>indeterminate</td>
                    <td>string</td>
                    <td></td>
                    <td><strong>This cannot be determined.</strong></td>
                  </tr>
                  <tr>
                    <td>name</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a name for the Switch Checkbox.</td>
                  </tr>
                  <tr>
                    <td>required</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies that a select element option must be selected to pass validation.</td>
                  </tr>
                  <tr>
                    <td>value</td>
                    <td>string</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>label</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears in the label above the Switch Checkbox element.</td>
                  </tr>
                  <tr>
                    <td>onClick</td>
                    <td>func</td>
                    <td></td>
                    <td>Function to be called when the Switch Checkbox is clicked.
                    </td>
                  </tr>
                  <tr>
                    <td>classname</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the Switch Checkbox.
                    </td>
                  </tr>
                  <tr>
                    <td>error</td>
                    <td>string</td>
                    <td></td>
                    <td>When a string is set on error the string is used as an error message to feed back to the user why the form has failed validation.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <hr />

          <h5>Button</h5>
          <div className="row">
            <div className="col5">
              <h6>Preview</h6>
              <div>

                <Button
                  label="Button Component"
                  iconClass="ion-help-circled"
                  linkUrl="http://bfy.tw/Bzku"
                >
                  HTML5 BUTTON
                </Button>

              </div>
              <h6>Description</h6>
              <div>
                <p>Standard HTML 5 Button element with enhancements such as error messaging, labels, icons and more.</p>
              </div>
            </div>
            <div className="col7">
              <h6>Usage (JSX)</h6>

              <Code>
                &lt;Button<br/>
                &nbsp;&nbsp;label=&quot;Button Component&quot;<br/>
                &nbsp;&nbsp;iconClass=&quot;ion-help-circled&quot;<br/>
                &nbsp;&nbsp;linkUrl=&quot;http://bfy.tw/Bzku&quot;<br/>
                &gt;<br/>
                &nbsp;&nbsp;HTML5 BUTTON<br/>
                &lt;/Button&gt;<br/>
              </Code>

            </div>
          </div>
          <div className="row">
            <h6>Props</h6>
            <div className="col12">
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Name</th>
                    <th width="10%">Type</th>
                    <th width="10%">Default</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>autoFocus</td>
                    <td>bool</td>
                    <td>false</td>
                    <td>Preselects focus to this element on page load if this is set to true.</td>
                  </tr>
                  <tr>
                    <td>classname</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the form submit button.</td>
                  </tr>
                  <tr>
                    <td>defaultValue</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>disabled</td>
                    <td>bool</td>
                    <td></td>
                    <td>Specifies whether the Button element should be disabled.</td>
                  </tr>
                  <tr>
                    <td>form</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies one or more forms the button belongs to.</td>
                  </tr>
                  <tr>
                    <td>formAction</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies the URL of the file that will process the input control when the form is submitted.</td>
                  </tr>
                  <tr>
                    <td>formEncType</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies how the form-data should be encoded when submitting it to the server</td>
                  </tr>
                  <tr>
                    <td>formMethod</td>
                    <td></td>
                    <td></td>
                    <td>Defines the HTTP method for sending data to the action URL (GET or POST)</td>
                  </tr>
                  <tr>
                    <td>formNoValidate</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies that form elements should not be validated when submitted</td>
                  </tr>
                  <tr>
                    <td>formTarget</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies where to display the response that is received after submitting the form</td>
                  </tr>
                  <tr>
                    <td>fullwidth</td>
                    <td>bool</td>
                    <td></td>
                    <td>When set to true, the button will fill the entire width of the containing element.</td>
                  </tr>
                  <tr>
                    <td>iconClass</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a icon assigned to a classname to apply to the button.</td>
                  </tr>
                  <tr>
                    <td>linkUrl</td>
                    <td>string</td>
                    <td></td>
                    <td>URL the button should load upon click.</td>
                  </tr>
                  <tr>
                    <td>name</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies a name for the Button.</td>
                  </tr>
                  <tr>
                    <td>value</td>
                    <td>string</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>label</td>
                    <td>string</td>
                    <td></td>
                    <td>Sets the text that appears in the label above the button element.</td>
                  </tr>
                  <tr>
                    <td>onClick</td>
                    <td></td>
                    <td></td>
                    <td>Function to be called when the Button is clicked.</td>
                  </tr>
                  <tr>
                    <td>classname</td>
                    <td>string</td>
                    <td></td>
                    <td>Specifies classname(s) to be applied to the Button element.</td>
                  </tr>
                  <tr>
                    <td>error</td>
                    <td>string</td>
                    <td></td>
                    <td>When a string is set on this error property the string is used as an error message to feed back to the user why selected option has failed validation.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <hr />

        </div>
      </div>
    );
  }
}

ComponentsPage.propTypes = {
};


export default ComponentsPage;
