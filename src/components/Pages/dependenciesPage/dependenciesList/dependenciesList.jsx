import React from 'react';
import PropTypes from 'prop-types';

import Dependency from './dependency.jsx'

import styles from './dependency.scss'

class DependenciesList extends React.Component {
  render() {

    let DepArray = Object.entries(this.props.dependencies).map( ([k, v]) => ({ name:k, version:v }) );
    //console.log(DepArray)
    
    let DependenciesList = DepArray.map((dependency) =>
      <Dependency 
        name={dependency.name}
        version={dependency.version}
        key={dependency.name}
      />
    )
 
    return(
      <table className={styles.dependancyList}>
        <thead>
          <tr>
            <th>Module Name</th>
            <th>Version Number</th>
          </tr>
        </thead>
        <tbody>
          {DependenciesList}
        </tbody>
      </table>
    );
  }
}

DependenciesList.propTypes = {
};
export default DependenciesList;