import React from 'react';
import PropTypes from 'prop-types';

import styles from './dependency.scss';

class Dependency extends React.Component {
  render() {    
    return(
      <tr className={styles.dependancy}>
        <td className={styles.dependancyNameCell}>
          <a href={`https://www.npmjs.com/package/${this.props.name}`} >
            <span className={styles.name}>{this.props.name}</span>
          </a>
        </td>
        <td className={styles.dependancyVersionCell}>
          <a href={`https://www.npmjs.com/package/${this.props.name}`} >
            <span className={styles.version}>{this.props.version}</span>
          </a>
        </td>
      </tr>
    );
  }
}

Dependency.propTypes = {
};
export default Dependency;