import React from 'react';
import PropTypes from 'prop-types';
import DependenciesList from './dependenciesList/dependenciesList.jsx';

import styles from './dependenciesPage.scss';


const DependenciesPage = () => 
  <div className={styles.pageContainer}>
    <section className={styles.detailsSection}>
      <h1><span className="ion-nuclear"></span><strong>Dependencies</strong></h1> 
      <p>Application Dependancies are used by the project and as such need to be included in your bundle, these are merged together in a seperate bundle file called vendor.js which is utilised to store any common libraries used in your application.</p>
      <p>Development Dependencies are not included in the bundled production files, they mostly comprise of modules used in this developement environment (such as CSS modules) and those that transpile our ES6 code into the ES5 javascript (bundle.js) so that it can be read by all browers. To find out more about a module, click its name in the depenencies table to open a link to its NPM page, often the best place to start when looking for detailed usage examples and documention.</p>
    </section>
    <section className={styles.depSection}>
      <h6>Application Dependencies</h6>
      <p>Dependancies used by this application:</p>
      <div>
        <DependenciesList 
          dependencies={CATALYST.DEPENDENCIES}
        />
      </div>
    </section>
    <section className={styles.devDepSection}>
      <h6>Development Dependencies</h6>
      <p>Dependancies used by the Development Environment:</p>
      <div>
        <DependenciesList 
          dependencies={CATALYST.DEVDEPENDENCIES}
        />
      </div>
    </section>
  </div>

DependenciesPage.propTypes = {
};
export default DependenciesPage;