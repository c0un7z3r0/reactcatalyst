import React from 'react';
import PropTypes from 'prop-types';

import Bugger from '../../common/bugger/bugger.jsx'
import Linky from '../../common/linky/linky.jsx'

import HeroFeature from './heroFeature/heroFeature.jsx';

import styles from './homePage.scss'; 

const HomePage = () => 
  <div className={styles.homePageContainer}>
    <div className={styles.asideSection}>
      <h1><span className="ion-ios-home"></span><strong>React Catalyst</strong></h1> 
      
      <p>React Catalyst was designed to allow the developer to quickly start creating 
      web applications using React.js, Redux, ReactRouter 4, SCSS/CSS Modules and more. 
      It features hot-reloading during development and has a highly optimised production 
      build system to ensure the smallest possible javascript output bundle.</p>
      
      <p>It is opinionated about the toolset employed as I built it for my own purposes,
      as such it has out of the box support for a huge range of development tools and 
      libraries including React, Redux, ReactRouter, CSS Modules, Autoprefix, SCSS, 
      Layout, Animation, Typography, Linting, Testing and more. It can also be configured 
      to deploy the optimised production build via FTP for easy itterative design with 
      rapid deployment.</p>

      <p>Over time, as I have used this on various projects, it has expanded to include 
      a broad range of commonly used reusable components. It features a range of controlled 
      components for forms, buttons, links, liteboxes and thumbnails. Each of these optional 
      reusable components has been built with simple configuration and optimisation in mind 
      and are only bundled as part of the final build if utilised in the project. This 
      greatly decreases development time and provides a wealth of helper components 
      off-the-shelf without additional redundant code making it into the final production 
      build - as is often the case when using external libraries.</p>
    </div>
    <div className={styles.contentSection}>
      
      <HeroFeature />

    </div>
  </div>

HomePage.propTypes = {
};
export default HomePage;
//export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
