import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../common/Button/button.jsx';

import styles from './reactContent.scss';

const ReactContent = () => 
  <div className={"col12 " + styles.contentContainer}>
    <div className="row">
      <h5>React.js - JS Library for building UI</h5>
    </div>
    <div className="row">
      <div className="col8">
        <p>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.</p>
        <p>Build encapsulated components that manage their own state, then compose them to make complex UIs.</p>
      </div>
      <div className="col3 align-center">

        <Button
          iconClass="ion-share"
          linkUrl="https://facebook.github.io/react/"
          fullwidth
          >
          Official React Homepage
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="https://medium.com/learning-new-stuff/learn-react-js-in-7-min-92a1ef023003"
          fullwidth
          >
          8-Min React Introduction
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="https://egghead.io/courses/react-fundamentals"
          fullwidth
          >
          Building React Apps
        </Button>
        
      </div>
    </div>
  </div>

export default ReactContent;
