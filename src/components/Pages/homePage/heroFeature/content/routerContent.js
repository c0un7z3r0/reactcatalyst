import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../common/Button/button.jsx';
import styles from './routerContent.scss';

const RouterContent = () => 
  <div className={"col12 " + styles.contentContainer}>
    <h5>React-Router v4 - A Navigational Components Library</h5>

    <div className="row">
      <div className="col8">
        <p>React Router is a library that enables you to render based on url strings and provides modular components to declare your UI based on the URL.</p>
      </div>
      <div className="col3">
      
        <Button
          iconClass="ion-share"
          linkUrl="https://reacttraining.com/react-router/web/guides/quick-start"
          fullwidth
          >
          Official React Router 4 Documentation
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf"
          fullwidth
          >
          Simple React Router v4 Tutorial
        </Button>

      </div>
    </div>
  </div>

export default RouterContent;
