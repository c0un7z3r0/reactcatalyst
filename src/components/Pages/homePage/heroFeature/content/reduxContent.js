import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../common/Button/button.jsx';

import styles from './reduxContent.scss';

const ReduxContent = () => 
  <div className={"col12 " + styles.contentContainer}>
    <div className="row">
      <h5>Redux - Unidirectional State Container</h5>
    </div>
    <div className="row">
      <div className="col8">
        <p>Redux is a predictable state container for JavaScript apps. </p>
        <p>It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test. On top of that, it provides a great developer experience, such as live code editing combined with a time traveling debugger.</p>
      </div>
      <div className="col3 align-center">
        <Button
          iconClass="ion-share"
          linkUrl="http://redux.js.org/"
          fullwidth
        >
          Official Redux Homepage
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="https://egghead.io/courses/getting-started-with-redux"
          fullwidth
        >
          Getting Started With Redux
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="https://medium.com/@stowball/a-dummys-guide-to-redux-and-thunk-in-react-d8904a7005d3"
          fullwidth
        >
          Redux &amp; Thunks
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="http://lorenstewart.me/2016/11/27/a-practical-guide-to-redux/"
          fullwidth
        >
          Practical Guide to Redux
        </Button>
        
      </div>
    </div>
  </div>

export default ReduxContent;
