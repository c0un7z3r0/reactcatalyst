import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../common/Button/button.jsx';
import styles from './webpackContent.scss';

const WebpackContent = () => 
  <div className={"col12 " + styles.contentContainer}>
    <h5>Webpack 4 - A module bundler for modern JavaScript applications</h5>

    <div className="row">
      <div className="col8">
        <p>Webpack is a module bundler that takes javascript modules and other files that comprise your application and bundles them into a single javascript file, mainly used for web app minification and bundling it can also be used to compile mobile apps and more.</p>
      </div>
      <div className="col3">
        <Button
          iconClass="ion-share"
          linkUrl="https://webpack.github.io/"
          fullwidth
          >
          Official Webpack Homepage
        </Button>

        <Button
          iconClass="ion-share"
          linkUrl="https://blog.madewithenvy.com/getting-started-with-webpack-2-ed2b86c68783"
          fullwidth
          >
          Getting Started With Webpack
        </Button>
      </div>
    </div>
  </div>

export default WebpackContent;
