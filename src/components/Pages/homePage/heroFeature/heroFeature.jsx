import React from 'react';
import PropTypes from 'prop-types';
import styles from './heroFeature.scss';

import reactIcon from './images/react-icon.png';
import reduxIcon from './images/redux-icon.png';
import routerIcon from './images/router-icon.png';
import webpackIcon from './images/webpack-icon.png';

import ReactContent from './content/reactContent.js';
import ReduxContent from './content/reduxContent.js';
import RouterContent from './content/routerContent.js';
import WebpackContent from './content/webpackContent.js';

class HeroFeature extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      active:"react"
    };

    this.onSelect = this.onSelect.bind(this);
    this.classnameGen = this.classnameGen.bind(this);
  }

  onSelect(selected){
    this.setState({
      active: selected
    });
  }

  classnameGen(item){
    if(item === this.state.active){
      return (styles.tabCell + ' ' + styles.heroSelected);
    }else{
      return (styles.tabCell);
    }
  }

  render() {

    let contentSwitcher = {};

    switch(this.state.active) {
      case "react":
        contentSwitcher = <ReactContent />;
        break;
      case "redux":
        contentSwitcher = <ReduxContent />;
        break;
      case "router":
        contentSwitcher = <RouterContent />;
        break;
      case "webpack":
        contentSwitcher = <WebpackContent />;
        break;

      default:
        contentSwitcher = <ReactContent />;
    }


    //TODO: ERRORS: JSX props should not use arrow functions  react/jsx-no-bin
    // Fix that with this:
    // https://stackoverflow.com/questions/36677733/why-jsx-props-should-not-use-arrow-functions

    return (
      <section className={styles.featuresWidgetContainer} id="#main-features">
        <div  className={"row " + styles.tabRow}>
          <div className={this.classnameGen('react')}>
            <img className={styles.heroFeatureTab} src={reactIcon} onClick={() => {this.onSelect('react');}}/>
          </div>
          <div className={this.classnameGen('redux')}>
            <img className={styles.heroFeatureTab} src={reduxIcon} onClick={() => {this.onSelect('redux');}}/>
          </div>
          <div className={this.classnameGen('router')}>
            <img className={styles.heroFeatureTab} src={routerIcon} onClick={() => {this.onSelect('router');}}/>
          </div>
          <div className={this.classnameGen('webpack')}>
            <img className={styles.heroFeatureTab} src={webpackIcon} onClick={() => {this.onSelect('webpack');}}/>
          </div>
        </div>
        <div className={"row " + styles.contentRow}>
          {contentSwitcher}
        </div>
      </section>
    );
  }
}

export default HeroFeature;
