import React from 'react';
import PropTypes from 'prop-types';
import classNamesModule from 'classnames/bind';
import styles from './sideNav.scss';

// Bind classnames to use css-modules
let classnames = classNamesModule.bind(styles);

class SideNav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeMenu: false,
      inactive: false,
      hovering: false
    };

    this.toggleActive = this.toggleActive.bind(this);
    this.onHovering = this.onHovering.bind(this);
  }

  toggleActive(){
    const currentState = this.state.activeMenu;

    if (currentState){
      // If the menu is OPEN
      this.setState({
        activeMenu: false
      });
      // set a 400ms delay to prevent hover issues (see css transition)
      setTimeout(() => {
        this.setState({ inactive: true });
      }, 400);

    }else{
      // If the menu is CLOSED
      // just TOGGLE them both immediately
      this.setState({ activeMenu: true,inactive: false });
    }
  }

  onHovering(){
    const currentState = this.state.hovering;
    this.setState({ hovering: !currentState});
  }

  render() {
    let articleNavClasses = classnames({
      articleNav: true,
      open: this.state.activeMenu,
      inactive: this.state.inactive,
      hovering: this.state.hovering
    });

    return(
      <div className={styles.sideMenuContainer}>
        <nav className={articleNavClasses}>

          <div className={styles.toggleButton}
               onClick={this.toggleActive}
               onMouseEnter={this.onHovering}
               onMouseLeave={this.onHovering}
          >
            <span className={styles.toggleButtonSymbol}
                  >
              &#10148;
            </span>
          </div>

          <ol className={styles.articleMenu}>
            <li>Purpose</li>
            <li>Getting Started
              <ul>
                <li>Requirements</li>
                <li>Download</li>
                <li>Install</li>
                <li>Usage</li>
              </ul>
            </li>
            <li>Dependancies</li>
            <li>Optional Components
              <ul>
                <li>Button</li>
                <li>SelectInput</li>
                <li>SubmitInput</li>
                <li>SwitchCheckbox</li>
                <li>TextArea</li>
                <li>TextInput</li>
              </ul>
            </li>
            <li>CSS Modules &amp; Libraries</li>
            <li>Grid System</li>
          </ol>
        </nav>
      </div>
    );
  }
}

SideNav.propTypes = {
   // myProp: PropTypes.string.isRequired
};

export default SideNav;
