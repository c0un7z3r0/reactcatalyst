import React from 'react';
import PropTypes from 'prop-types';

import SideNav from './sideNav/sideNav.js';
import Code from '../../common/code/code.jsx';
import Button from '../../common/Button/button.jsx';

import styles from './documentationPage.scss';

//import DependancyWidget from './dependancyWidget/dependacyWidget.js';

import packageJson from '../../../../package.json';

const catalystDownloadLink = 'ReactCatalyst-v' + packageJson.version + '.zip';

const CSScodeblock1 = "import React from \'react\';\r\nimport PropTypes from \'prop-types\';\r\nimport classNamesModule from \'classnames\/bind\';\r\nimport styles from \'.\/sideNav.scss\';\r\n\r\n\/\/ Bind classnames to use css-modules\r\nlet classnames = classNamesModule.bind(styles);\r\n\r\n\r\nclass SideNav extends React.Component {\r\n\r\n...\r\n\r\n  render() {\r\n    let articleNavClasses = classnames({\r\n      articleNav: true,\r\n      open: this.state.activeMenu,\r\n      inactive: this.state.inactive,\r\n      hovering: this.state.hovering\r\n    });\r\n\r\n    return(\r\n      <div className={styles.sideMenuContainer}>\r\n        <nav className={articleNavClasses}>\r\n\r\n...\r\n\r\n        <\/nav>\r\n      <\/div>\r\n    );\r\n  }\r\n}\r\n\r\n...\r\n\r\nexport default SideNav;";

const CSScodeblock2 = "...\r\n\r\nimport \'.\/static\/styles\/styles.scss\';\r\nimport \'.\/static\/styles\/grid.scss\';\r\nimport \'.\/static\/styles\/my-theme\/base.scss\';\r\nimport \'.\/static\/styles\/my-theme\/typography.scss\';\r\nimport \'.\/static\/styles\/my-theme\/scrollbars.scss\';\r\n\r\n...";


const CSScodeblock3 = "...\r\n\r\nimport styles from \'.\/heroFeature.scss\';\r\n\r\n...\r\n\r\n  render() {\r\n    return (\r\n      <section className={styles.featuresWidgetContainer} id=\"#main-features\">\r\n        <div  className={\"row \" + styles.tabRow}>\r\n          \r\n        ...\r\n\r\n        <\/div>\r\n        <div className={\"row \" + styles.contentRow}>\r\n          \r\n        ...\r\n\r\n        <\/div>\r\n      <\/section>\r\n    );\r\n  }\r\n\r\n  ...";



class AboutPage extends React.Component {
  render() {
    return(
      <div className={styles.documentationPageContainer}>
        <div className={styles.asideSection}>
          <h1><span className="ion-ios-book"></span><strong>Documentation</strong></h1> 
          <ol className={styles.articleMenu}>
            <li>Purpose</li>
            <li>Getting Started
              <ul>
                <li>Requirements</li>
                <li>Download</li>
                <li>Install</li>
                <li>Usage</li>
              </ul>
            </li>
            <li>CSS Modules &amp; Libraries</li>
            <li>React Router</li>
            <li>Version Number Control</li>
            <li>Grid System</li>
          </ol>
        </div>
        <div className={styles.contentSection}>

          {/* <SideNav /> */}

          <div className="row">
            <div className="col12">
              <article>
                <h1><strong>Documentation</strong></h1>
                <h4>ReactCatalyst - A React/Redux based Development Environment</h4>
                <hr />

                <section id="#purpose" className="indent2 col10">
                  <h3>Purpose</h3>
                  <p>React Catalyst is a modern javascript development environment, sometimes refered to as a starter kit. It provides tooling for developers to quickly create a javascript application for the web using web standard HTML5, S/CSS and React.js. It utilises Webpack 4 to gather your source code and bundles it to create compressed javascript files ready for use on the web. It then bundles these together along with your site files ready for easy deployment via FTP. This particular starter kit uses the wildly popular javascript library, React.js as its core rendering engine and also comes with redux and react-router pre-configured, although these are optional and easily removed should they not be needed for your application or website.</p>
                  <p>Features</p>
                  <div className="row">
                    <div className="indent1 col5">
                      <ul>
                        <li>Hot Reload in Dev Environment</li>
                        <li>CSS Libraries</li>
                        <li>CSS-Modules</li>
                        <li>SASS - Full Support</li>
                        <li>Optimised Production Bundling</li>
                        <li>FTP Deployment Support</li>
                      </ul>
                    </div>
                    <div className="col5">
                      <ul>
                        <li>ES6 Syntax Babel-transpiled to ES5</li>
                        <li>React Router 4</li>
                        <li>Redux</li>
                        <li>ESLint Code Check - As You Work</li>
                        <li>Mocha Testing - TDD Ready</li>
                        <li>Rapid Deployment Focused</li>
                      </ul>
                    </div>
                  </div>
                </section>

                <hr/>

                <section id="#gettingstarted" className="indent2 col10">
                  <h3>Getting Started</h3>

                  <section id="#gettingstarted-requirements">
                    <h6>1. Requirements</h6>
                    <p>React catalyst bundles with webpack 4 which requires <strong><a href="https://nodejs.org/">node.js</a></strong> so you will need this installed to provide the javascript modules we will be using to create our application. As we use the ES6 syntax in this starter pack I've had no problem using the 'Current: Latest Version' of node.js although the 'LTS Recommended Version' is supposedly more stable. Follow the link below and choose the download for your platform and install node.js:</p>
                    <div className="indent1 col10 align-right">
                      <Button
                        iconClass="ion-social-nodejs"
                        linkUrl="https://nodejs.org/en/download/"
                      >
                        Node.js Download
                      </Button>
                    </div>
                  </section>

                  <section id="#gettingstarted-download">
                    <h6>2. Download</h6>
                    <p>You can download ReactCatalyst from bitbucket with Git:</p>
                    <div className="indent1 col10">
                      <Code>
                        git clone https://c0un7z3r0@bitbucket.org/c0un7z3r0/reactcatalyst.git
                      </Code>
                    </div>
                    <p>Alternatively you can download a zip containing the source from the live-demo of this starter pack hosted at: </p>
                    <div className="indent1 col10 align-right">
                      <Button
                        iconClass="ion-android-download"
                        linkUrl={catalystDownloadLink}
                      >
                        ReactCatalyst
                      </Button>
                    </div>
                  </section>

                  <section id="#gettingstarted-install">
                    <h6>3. Install</h6>
                    <p>Once you have the source file downloaded to your project directory, using your command line, install the dependencies with the following command:</p>
                    <div className="indent1 col10">
                      <Code>
                        npm install
                      </Code>
                    </div>
                    <p>Thats all. <a href="https://www.npmjs.com/">Node Package Manager</a> will install the required modules. Once that has completed you are ready to get started.</p>
                  </section>

                  <section id="#gettingstarted-usage">
                    <h6>4. Usage</h6>

                    <p><strong>Development Environment</strong></p>
                    <p>This react development environment has a development server which enables you to see your code changes on the fly. To start the development environment and get started type:</p>
                    <div className="indent1 col10">
                      <Code>
                        npm start
                      </Code>
                    </div>
                    <p>The dev environment bundle is specifically setup to enable you to make code changes and not have to restart the build process every time, it also inlines all the styles and other tricks which mean that <u>the development bundle should not be used in a production environment.</u></p>

                    <p><strong>Production Build</strong></p>
                    <p>Once you have completed writing your application and you wish to run a minified production-ready build you must stop your dev environment (ctrl+c) then simply run the build script with the following command:</p>
                    <div className="indent1 col10">
                      <Code>
                        npm run build
                      </Code>
                    </div>
                    <p>This will create a folder called <span className="code">/dist/</span>  and run an optimised bundle process which will minify the app into two javascript files: vendor.js and main.js. Vendor.js has common libraries such as react, react-router and other production dependancies. This leverages browser caching to ensure that any subsequent updates to the app wont require a whole new bundle to be downloaded, just the updated section in your code which is exported to Main.js. </p>
                    <p>The build also processes all css libraries from the <span className="code">/static/</span> folder, all scss/css-modules and minifies it into a single css file and places them into the <span className="code">/dist/styles/</span> folder. It also generates a HTML file based on the template in <span className="code">/static/</span>, copies any fonts, images and other files used in the application are also placed in the <span className="code">/dist/</span> folder ready to be deployed.</p>
                    <p>Once the minified bundle is generated a report is run that offers a file-size analysis of your bundles to help with further optimisation and bloat reduction.</p>
                    <p>To enable you to manually test your production build a dist server is fired up. Both your applications production build and the file-size analysis are automatically opened in your default browser upon completion.</p>

                    <p><strong>FTP Deployment</strong></p>
                    <p>React Catalyst has the ability to deploy your completed and bundled app and all its files in the <span className="code">/dist/</span> to a remote server via FTP. </p>
                    <p>To enable this feature simply populate the <span className="code">envConfig.js</span> in the root with your username, password and FTP address. You can optionally specify additional files to include/exclude, specify a destination folder ("/" by default) and change the default port number.</p>

                    <p>Once you are satisfied with your production build, you've tested it in the browser, checked the file-size report you may now wish to deploy it to a web server. You have the option of deploying from the command line via FTP</p>
                    <p>You will of course need to add the FTP details for your particular server, these can be configured in the <span className="code">envConfig.js</span> file in the root folder, add your own Username, Password, FTP address, Destination directory and Port number and you can simply run the deploy task whenever you wish to push to server with the following command:</p>
                    <div className="indent1 col10">
                      <Code>
                        npm run deploy
                      </Code>
                    </div>
                    <p>This re-runs the Production Build process (clears <span className="code">/dist/</span> folder, generates new app bundle and files - the same as Production Bundle) except it does not fire up the dist server. Instead it deploys all the files built and pushes them via FTP to the server specified in the <span className="code">envConfig.js</span> file.</p>
                    <p>This task also produces the file-size report in the <span className="code">/dist/</span> folder however an exception for this is added in <span className="code">envConfig.js</span> to prevent this from being uploaded along with your production code. You can add your own exceptions (and any additional files you wish to include outside of the <span className="code">/dist/</span> folder) in the <span className="code">envConfig.js</span> as required.</p>
                  </section>

                </section>

                <hr />

                <section id="#dependancies" className="indent2 col10">
                  <div>
                    <h3>Stylesheets</h3>
                    <section>
                      <p>React Catalyst supports standard SCSS and CSS libraries as well as <a href="https://glenmaddern.com/articles/css-modules">CSS-Modules</a>. To enable the use of both CSS libraries and CSS modules we use a convention to determine which CSS/SCSS files should be treated as modules and which should be libraries. This is required because CSS modules uses a BEM naming scheme to rename your selectors to ensure they are limited to the scope of the component your CSS module is targeting.</p>
                      <p>For instance, if you take a look at the sourcecode of the <span className="code">code.js</span> component you will see the containing div has the following code:</p>
                      <p><span className="code">&lt;div className=&#123;styles.codeContainer&#125;&gt;</span></p>
                      <p>This will be renamed by css-modules to output the following html:</p>
                      <p><span className="code">&lt;div class="code__codeContainer___2epia"&gt;</span></p>
                      <p>This first segment of this outputted name is the name of the component (code__), the second segment is the classname given (...codeContainer___)and the third segment is a hash based on the properties used (...2epia). This is done to ensure that the scope of this style rule is limited to the component it is being used on ONLY. These styles will not be inherited by anything else called 'code' or 'codeContainer' because it is scoped to this instance alone due to the elaborate classname.</p>
                      <p>This solves a great many problems that come with the global scope nature of css, it allows us to take this component (and its css module) and drop it into another project and it will have its styles and you can be confident that the style properties wont be inherited by other components or elements. Its a great solution but this also presents us with a new problem when it comes to using CSS Libraries - the classnames in our JSX (component__classname___hash) wont match those declared in the library because they have been renamed. For instance if we were to import a css library, bootstrap.css using the method above we would need to preprocess the library to rename all of the classes in the library to match those used in our app outputted in the BEM naming convention described above. It could also potentially result in many declarations of the same style on multiple classnames which is wasteful and defeats the purpose.</p>
                      <p> We can solve this by declaring exceptions in our webpack config so that any css libraries that are imported from either the <span className="code">/static/styles</span> folder or the <span className="code">/node_modules/</span> should NOT be transformed, their classnames will remain as they are in the libraries and should be named as such in your source code.</p>
                      <p>This method gives us the best of all worlds, we can use modular CSS on a component level while still being able to use global-scope css libraries. You can use the classnames module to utilise both of these, an example of this in use can be seen in the <span className="code">button.js</span> component (below), amongst others.</p>
                      <h5>Usage differences between CSS Modules and CSS Libraries</h5>
                      <p>As previously mentioned, we can use the popular classnames module to conditionally show and group our classnames depending on the state of our app or props passed to our component.</p>

                      <h6>CSS Modules</h6>
                      <p>Here is an example of CSS modules in use, this was taken from the <span className="code">sideNav.js</span> component on this page</p>

                      <Code>
                        {CSScodeblock1}
                      </Code>

                      <p>First, its worth noting the import statement here. In order to utilise CSS Modules you should name your import, in this case we have used <span className="code">import styles from 'sideNav.scss'</span>. If you had a class called <span className="code">.widgetContainer</span> in this stylesheet, you would refer to it using <span className="code">&#123;styles.widgetContainer&#125;</span></p>
                      <p>On line 7 you can see that we import the 'classnames' module using the bind method. This popular module is used to determine which classnames to use based on your apps state or some logic in your code. We use the bind method in this case so that 'classnames' knows to utilise the BEM naming convention used when generating the CSS rules from our css modules files. </p>
                      <p><span className="code">let classnames = classNamesModule.bind(styles);</span></p>
                      <p>Then, in the render method, we create a variable called articleNavClasses which determines which classnames are used based on the state of the component. For instance, if the user has the menu open, then <span className="code">this.state.activeMenu</span> will be set to <span className="code">true</span> so in this case, classnames will use the classname <span className="code">'open'</span> which, with bind, is equivelent to <span className="code">'styles.open'</span>. When that is processed by css modules and transformed to use the BEM naming convention, it will result in the following class in your HTML <span className="code">sideNav__open___sjiRY</span></p>

                      <h6>CSS Libraries</h6>
                      <p>Libraries are just traditional CSS. To include a CSS library you should import it into your project with <span className="code">import '../path/to/csslibraryname.css';</span>. In order to use third party CSS libraries, you must import them into your project from either the <span className="code">static/styles</span> folder or from your <span className="code">node_modules</span> folder, you should not name your imports. </p>
                      <p>CSS libraries imported from either of these locations will not be BEM transformed, so for instance if you imported animate.css in order to use the <span className="code">'Bounce'</span> animation. Rather than using <span className="code">className = &#123;styles.bounce&#125;</span> as the classname in your JSX - as you would with CSS modules, instead you would just use regular classname declarations, like so: <span className="code">className = "bounce"</span> </p>
                      <p>Using this method prevents you from having to manually convert all used css libraries to utilise the BEM naming convention required for CSS Modules, with this method you get all of the benefits of both CSS Modules and CSS Libraries with none of the drawbacks of either methodology. You get the limited scope benefits of CSS modules while retaining the ability to use global scope where required in CSS libraries!</p>
                      <p>NOTE: Stylesheets are handled slightly differently in the dev environment. To enable hot-reloading of changes made to your CSS module/libraries, webpack compiles the css from both the modules and libraries used and puts them inline in the <span className="code">&lt;head&gt;</span>  tag, in <span className="code">&lt;style&gt;</span>  tags.</p>
                      <p> CSS Libraries can be imported into any file with <span className="code">import 'someLibrary.css'</span>, although it is a good idea to keep all of your global CSS imports in one place. For example, in this documentation application, all css libraries are imported within the <span className="code">app.js</span> file. When the production bundle job is run all CSS modules and libraries are optimised and minified into one file: <span className="code">style.css</span> this is placed in a <span className="code">/styles/</span> folder in the <span className="code">/dist/</span> ready for deployment to production. </p>
                      <p>Here is an example of a CSS library import:</p>
                      <p>app.js</p>
                      <Code>
                        {CSScodeblock2}
                      </Code>
                      <p><sup>Note: For CSS libraries, naming the import is not required as it is with CSS Modules.</sup></p>
                      <p>heroFeature.js</p>
                      <Code>
                        {CSScodeblock3}
                      </Code>
                      <p><sup>Note: The imported heroFeature.scss file is a CSS module, hence it has a named import (styles).</sup></p>
                      <p>As you can see, the global CSS libraries are imported from the top-level <span className="code">app.js</span> and are utilised in this child component: <span className="code">heroFeature.js</span>. If you look at line 10 of the heroFeature.js code, you will see that we utilise both a CSS Libraries classname <span className="code">row</span> and a CSS Modules classname <span className="code">styles.tabRow</span> inline.</p>
                      <p> If you are only utilising a couple of classnames, doing them inline is fine however if you are handling multiple conditional classnames then you may find the 'classnames' NPM module used in the CSS modules example (above) easier to manage.</p>
                      <p>How these are utilised is entirely up to the developer, but the important thing to note is the syntax to use in each case:</p>
                      <p>CSS Modules:</p>
                      <Code>
                        import styles from "myCSSmodule.scss"<br/>
                        ...<br/>
                        style=&#123;styles.myclass&#125;
                      </Code>
                      <p>CSS Libraries</p>
                      <Code>
                        import "myCSSlibrary.css"<br/>
                        ...<br/>
                        style="myclass"
                      </Code>
                      <p>SCSS and CSS are completely interchangable in css modules, as long as a scss file has the correct extention then it will be bundled correctly.</p>

                      <h6>Optional CSS Libraries Included</h6>
                      <p>Included in React Catalyst are a few helpful CSS Libraries:</p>
                      <p><strong>Normalize.css</strong></p>
                      <p>Normalize.css is a tiny stylesheet which ensures all elements are rendered consistently across all browsers. This gives you an ideal starting point for your application because with normalize.css included in your project you can be confident that your application will render consistently across all browsers. </p>
                      <p><strong>This library is strongly recommended for applications destined to be used in web browsers - mobile, tablet or desktop.</strong> It will save you so much pain further down the line, you wont have to retroactively tweak things to acheive cross-browser compatibility.</p>

                      <p><strong>Grid.css</strong></p>
                      <p>Grid is a simple CSS Library that enables the user to structure web pages and components into columns and rows. Using a simple 12 column grid system gives you full control over your layout.</p>
                      <p>Basic Usage:</p>
                      <p>Row</p>
                      <p><span className="code">&lt;div className="row"&gt;</span> creates a row that spans 100% of the width of the containing element. This simply provides a row for us to arrange our columns in.</p>
                      <p>Column classes come in varying sizes: <span className="code">.col1</span> to <span className="code">.col12</span> depending on the size required. For example <span className="code">&lt;div className="col12"&gt;</span> creates a div that spans the whole width of the row. Inversely <span className="code">&lt;div className="col1"&gt;</span> produces a div that spans only 1/12<sup>th</sup> of the row. So if you wanted to create a layout that was split into two even columns you would write:</p>
                      <Code>
                        &lt;div className="row"&gt;<br/>
                        &nbsp;&nbsp;&lt;div className="col6"&gt;<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;Column 1 Content<br/>
                        &nbsp;&nbsp;&lt;/div&gt;<br/>
                        &nbsp;&nbsp;&lt;div className="col6"&gt;<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;Column 2 Content<br/>
                        &nbsp;&nbsp;&lt;/div&gt;<br/>
                        &lt;/div&gt;<br/>
                      </Code>
                      <p>If you want to indent your column, rather than creating a column of empty space, you can simply use the indent classes provided: For instance <span className="code">.indent2</span> will indent 2 column spaces as illustrated in this example:</p>
                      <Code>
                        &lt;div className="row"&gt;<br/>
                        &nbsp;&nbsp;&lt;div className="indent2 col10"&gt;<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;Column 10 Content - with .indent2<br/>
                        &nbsp;&nbsp;&lt;/div&gt;<br/>
                        &lt;/div&gt;<br/>
                      </Code>
                      <p>This will produce a 10 column width div that is indented by 2 column blocks.</p>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#react-router" className="indent2 col10">
                  <div>
                    <h3>React Router v4</h3>
                    <section>
                      <p>React Catalyst uses React Router v4. This is the latest version of the wildly popular routing solution. It enables you to create a single page application that delivers ALL pages of your application in once bundle, as opposed to delivering each page when it is requested. This improves download speeds and drastically reduces the number of page requests. For more details on how React Router v4 works and how to utilise it please read the documentation:</p>
                      <div className="indent1 col10 align-right">
                        <Button
                          iconClass="ion-share"
                          linkUrl="https://reacttraining.com/react-router/web/guides/quick-start"
                        >
                          React Router v4
                        </Button>
                      </div>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#versionbump" className="indent2 col10">
                  <div>
                    <h3>Version Bump</h3>
                    <section>
                      <p>This starter kit has a version numbering system that can be incremented manually by using the following commands:</p>
                      <Code>npm run bump:patch</Code>
                      <p>Will increment the last number so that <span className="code">v1.0.3</span> would be 'bumped' to <span className="code">v1.0.4</span></p>
                      <Code>npm run bump:minor</Code>
                      <p>Will increment the middle number so that <span className="code">v1.0.4</span> would be 'bumped' to <span className="code">v1.1.4</span></p>
                      <Code>npm run bump:major</Code>
                      <p>Will increment the first number so that <span className="code">v1.1.4</span> would be 'bumped' to <span className="code">v2.1.4</span></p>
                      <p>When you start the production server the bump:patch task is run automatically so that when you commit your changes the version number is incremented to reflect the changes made although this can be edited manually at the top of the package.json file in the root as required.</p>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#localtunnel" className="indent2 col10">
                  <div>
                    <h3>Localtunnel</h3>
                    <section>
                      <p>Doesnt work at work on the macbook. Macbook or network issue? investigate - try both macbook and ultrabook at home.</p>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#devtools" className="indent2 col10">
                  <div>
                    <h3>Browser Dev Tool Support</h3>
                    <section>
                      <p>React Catalyst supports both React and Redux Developer tools, these useful debugging tools are available as Chrome or Firefox Extentions. Documentation on the usage of these tools is beyond the scope of this readme however more information on these great debugging tools can be found by clicking the links below, you can download for the browser of your choice by selecting the relevant icon:</p>
                      <p>
                        <a href="https://github.com/facebook/react-devtools">
                          React Devtools
                        </a>
                      </p>
                      <p>Fully featured React.js inspection including on-the-fly editing in the inspector.</p>
                      <div className="row">
                        <div className="indent1 col5">
                          <Button
                            iconClass="fa fa-firefox"
                            linkUrl="https://addons.mozilla.org/en-US/firefox/addon/react-devtools/"
                          >
                            React Devtools for Firefox
                          </Button>
                        </div>
                        <div className="indent1 col5">
                          <Button
                            iconClass="fa fa-chrome"
                            linkUrl="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en"
                          >
                            React Devtools for Chrome
                          </Button>
                        </div>
                      </div>
                      <p>
                        <a href="https://github.com/facebook/react-devtools">
                          Redux Devtools
                        </a>
                      </p>
                      <p>Redux Devtools provides application state editing features including Rewind and Fast-forward features to debug redux issues.</p>
                      <div className="row">
                        <div className="indent1 col5">
                          <Button
                            iconClass="fa fa-firefox"
                            linkUrl="https://addons.mozilla.org/en-US/firefox/addon/remotedev/"
                          >
                            Redux Devtools for Firefox
                          </Button>
                        </div>
                        <div className="indent1 col5">
                          <Button
                            iconClass="fa fa-chrome"
                            linkUrl="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en"
                          >
                            Redux Devtools for Chrome
                          </Button>
                        </div>
                      </div>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#icon-fonts" className="indent2 col10">
                  <div>
                    <h3>Icon Sets</h3>
                    <section>
                      <p>Included with this starter pack are two font-icon collections.</p>
                      <p><a href="http://ionicons.com/">Ionicons</a></p>
                      <p><a href="http://fontawesome.io/">Font Awesome</a></p>
                      <p> To utilise these icons, simply enable the collection you wish to use by uncommenting the relevant line from the <span className="code">src/styles/styles.scss</span> file:</p>
                      <Code>
                        ...<br/>
                        // SUGGESTED LIBRARIES (uncomment to include)<br/>
                        // -Ionicons<br/>
                        @import './ionicons/ionicons.scss';<br/>
                        // - Font-awesome Icons<br/>
                        @import './font-awesome/font-awesome.scss';<br/>
                        ...<br/>
                      </Code>
                      <p>You can then utilise these icons by applying the relevant class to a <span className="code">&lt;span&gt;</span> element like so:</p>
                      <Code>
                        &lt;span className=&quot;fa fa-firefox&quot; &gt;&lt;/span&gt;
                      </Code>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#icon-fonts" className="indent2 col10">
                  <div>
                    <h3>Animate.css</h3>
                    <section>
                      <p>Also included with this starter pack is the popular animate.css library. Again, usage of these animations depends on context and the documentation of CSS animation is beyond the scope of this document but you can see examples of these animations in action and more detailed documentation on the <a href="https://daneden.github.io/animate.css/">animate.css homepage</a> and for more information on CSS animation in general, visit <a href="https://www.w3schools.com/css/css3_animations.asp">W3Schools.</a></p>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#html-template" className="indent2 col10">
                  <div>
                    <h3>HTML Template</h3>
                    <section>
                      <p>The HTML template, <span className="code">index.html</span>, can be found in the <span className="code">/src/static</span> folder. This is used by the HTMLwebpackPlugin to generate the index.html file in our <span className="code">/dist/</span> folder. This webpack plugin supports templating engines such as EJS and Handlebars however the basic example used in this starter kit is simply HTML however it includes an example of how to set variables in your webpack config and use them conditionally in your HTML.</p>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#favicon" className="indent2 col10">
                  <div>
                    <h3>Favicons</h3>
                    <section>
                      <p>The html template is setup to utilise favicons for desktop, windows, apple and android ecosystems and should you wish to include them in your project all you need to do is generate an appropriate icon set using a generator, (I recommend <a href="http://www.favicon-generator.org/">www.favicon-generator.org</a>) then simply place the generated icon files into the <span className="code">/src/static/images/favicon</span> folder. When you run your production build they will be bundled for you in the <span className="code">/dist/</span> folder ready for deployment.</p>
                    </section>
                  </div>
                </section>

                <hr />

                <section id="#favicon" className="indent2 col10">
                  <div>
                    <h3>Typography</h3>
                    <section>
                      <p>A CSS library that provides typography styles is included and can be found at <span className="code">/static/styles/typography.scss</span>. The font utilised can be changed in the <span className="code">/static/styles/_variable.scss</span> file. </p>
                    </section>
                  </div>
                </section>

              </article>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AboutPage.propTypes = {
};
export default AboutPage;
