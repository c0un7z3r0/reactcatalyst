import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as dependancyActions from '../../../../actions/dependancyActions';
//import Linky from '../../common/linky/linky.js';
//import styles from './homePage.scss';
//
import DependancyFormContainer from './dependancyForm/dependancyFormContainer.js';
import DependancyList from './dependancyList/dependancyList.js';


class DependancyWidget extends React.Component {
  constructor(props, context) {
    super(props, context);

  }
  render() {
    return(
      <div>
        <DependancyList dependancies={this.props.dependancies} />
        <DependancyFormContainer dependancies={this.props.dependancies}/>

      </div>
    );
  }
}

DependancyWidget.propTypes = {
  dependancies: PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    dependancies: state.dependancies
  };
}


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(dependancyActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DependancyWidget);
