import React from 'react';
import PropTypes from 'prop-types';
//import Linky from '../../common/linky/linky.js';
//import styles from './homePage.scss';
import TextInput from '../../../../common/forms/textInput/textInput.jsx';
import SelectInput from '../../../../common/forms/selectInput/selectInput.jsx';
import TextArea from '../../../../common/forms/textArea/textArea.jsx';
import SubmitInput from '../../../../common/forms/submitInput/submitInput.jsx';
//import Button from '../../common/forms/Button/button.js';
//import SwitchCheckBox from '../../common/forms/switchCheckBox/switchCheckBox.js';

class DependancyForm extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {dependancy, onSave, onChange, saving, errors, dropdownOptions} = this.props;
    return(
      <form>
        <div className="row">

          <div className="col6">
            <TextInput
              name="title"
              key="title"
              label="Module"
              //value={dependancy.title}   <-- complains about controlled/uncontrolled components
              onChange={onChange}
              error={errors.title}
              placeholder="Name of the NPM Module"
              classname="NoClass"/>

            <TextInput
              name="watchHref"
              key="watchHref"
              label="NPM/GitHub Link"
              //value={dependancy.watchHref}
              onChange={onChange}
              error={errors.watchHref}
              placeholder="Link to Docs/Github Page"
              classname="NoClass"/>

            <SelectInput
              name="developDep"
              key="developDep"
              label="Develop/Production Dependancy"
              //value={dependancy.developDep}
              defaultOption="Choose: Dev / Production"
              options={dropdownOptions}
              onChange={onChange}
              error={errors.developDep}
              classname="TESTCLASS"/>
          </div>

          <div className="col6">
            <TextArea
              name="description"
              key="description"
              onChange={onChange}
              error={errors.description}
              //value={dependancy.description}
              rows="11"
              label="Description"
              maxlength="100"
              placeholder="A short description of this dependancy and its purpose for inclusion"
              wrap="hard"
              />
          </div>
        </div>

        <div className="row">
          <div className="indent10 col2 align-right">
            <SubmitInput
              type="submit"
              value={saving? 'SAVING' : 'SAVE'}
              className="btn btn-primary"
              onClick={onSave}
              />
          </div>
        </div>
      </form>
    );
  }
}

DependancyForm.propTypes = {
  dependancy: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  dropdownOptions: PropTypes.array.isRequired
};

export default DependancyForm;
