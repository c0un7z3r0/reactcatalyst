import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as dependancyActions from '../../../../../actions/dependancyActions';
import DependancyForm from './dependancyForm.js';



export class DependancyFormContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      dependancy: {title:'', description:'', developDep:'', watchHref:''},
      errors:{},
      saving: false
    };

    this.updateDependancyState = this.updateDependancyState.bind(this);
    this.saveDependancy = this.saveDependancy.bind(this);

    this.baseState = this.state;
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.dependancy.title !=nextProps.dependancy.title){
      this.setState({dependancy: Object.assign({},nextProps.dependancy)});
    }else if(nextProps === undefined){
      this.setState({dependancy: { title:'', description:'', developDep:'', watchHref:''}});
    }
  }

  updateDependancyState(event){

    const field = event.target.name;
    let dependancy = this.state.dependancy;
    dependancy[field] = event.target.value;
    return this.setState({dependancy: dependancy});
  }

  dependancyFormIsValid() {
    let formIsValid = true;
    let errors = {};

    if (this.state.dependancy.title.length < 5) {
      errors.title = 'Name must be at least 5 characters.';
      formIsValid = false;
    }

    if (this.state.dependancy.watchHref.length < 5) {
      errors.watchHref = 'Link must be at least 5 characters and be a valid URL.';
      formIsValid = false;
    }

    if (this.state.dependancy.developDep == ''){
      errors.developDep = 'Please choose if this is a development or production dependancy';
      formIsValid = false;
    }

    if (this.state.dependancy.description.length < 10) {
      errors.description = 'Description must be at least 10 Characters';
      formIsValid = false;
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  resetForm(){
    //this.setState({dependancy: { title:'', description:'', developDep:'', watchHref:''}});
    this.setState(this.baseState);
  }

  saveSuccessful(){
    //console.log('Saved!');

    // This doesnt reset the form, should that be done as part of the redux action or in the form controller?, get matts advice.
    //let dependancy = {
    //  title: '',
    //  watchHref: '',
    //  description: '',
    //  developDep: ''
    //};


    //this.setState({
    //  //dependancy:dependancy,
    //  saving:false
    //});

    // ...this dont work either.
    this.resetForm();
  }

  saveDependancy(event) {
    event.preventDefault();
    if (!this.dependancyFormIsValid()){
      return;
    }
    this.setState({saving:true});
    this.props.actions.saveDependancy(this.state.dependancy)
      .then(() => this.saveSuccessful())
      //console.log(this.state.dependancy.name+' Saved')
      .catch(error => {
        //toastr.error(error);
        this.setState({
          saving:false,
          errors: error
        });
      });
  }

  render() {
    return (
      <DependancyForm
        onChange={this.updateDependancyState}
        onSave={this.saveDependancy}
        dependancy={this.state.dependancy}
        dropdownOptions={this.props.dropdownOptions}
        errors={this.state.errors}
        saving={this.state.saving}
        />
    );
  }
}

DependancyFormContainer.propTypes = {
  dropdownOptions: PropTypes.array.isRequired,
  dependancy: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

DependancyFormContainer.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  //const dependancyId = ownProps.params.id; // from the path '/course/:id'

  let dependancy = {
    title: '',
    watchHref: '',
    description: '',
    developDep: ''
  };

  let dropdownOptions = [{value: 'develop',  text: 'Develop'},{value: 'production',  text: 'Production'}];

  return {
    dependancy: dependancy,
    dropdownOptions: dropdownOptions
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(dependancyActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DependancyFormContainer);
