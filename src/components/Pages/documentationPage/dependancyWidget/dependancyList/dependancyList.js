import React from 'react';
import PropTypes from 'prop-types';
import DependancyListRow from './dependancyListRow';

const DependancyList = ({dependancies}) => {
  return (
    <table className="table">
      <thead>
      <tr>
        <th>MODULE</th>
        <th>DESCRIPTION</th>
        <th>DEV/<wbr/>PROD</th>
        <th style={{minWidth:80}}>DOCS</th>
      </tr>
      </thead>
      <tbody>
      {dependancies.map(dependancy =>
          <DependancyListRow key={dependancy.title} dependancy={dependancy}/>
      )}
      </tbody>
    </table>
  );
};

DependancyList.propTypes = {
  dependancies: PropTypes.array.isRequired
};

export default DependancyList;
