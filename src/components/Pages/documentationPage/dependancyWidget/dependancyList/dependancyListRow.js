import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import styles from './dependancyListRow.scss';
import GithubIcon from './images/github-icon.png';
import NPMIcon from './images/npm--icon.png';

const DependancyListRow = ({dependancy}) => {
  return (
    <tr>
      <td><strong>{dependancy.title}</strong></td>
      <td>{dependancy.description}</td>
      <td>{dependancy.developDep ? 'Dev' : <strong>Prod</strong>}</td>
      <td>
        <a className={styles.iconLink} href={dependancy.watchHref} target="_blank">
          <img className={styles.iconImage} src={GithubIcon}/>
        </a>
        <a className={styles.iconLink} href={'https://www.npmjs.com/package/' + dependancy.title} target="_blank">
          <img className={styles.iconImage} src={NPMIcon}/>
        </a>
      </td>
    </tr>
  );
};

DependancyListRow.propTypes = {
  dependancy: PropTypes.object.isRequired
};

export default DependancyListRow;
