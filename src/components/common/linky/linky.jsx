import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const Linky = ({ activeClassName, to, className, children, exact }) => 
  <NavLink
    to={to}
    activeClassName={activeClassName}
    className={className}
    exact={exact}
  >
    {children}
  </NavLink>
  
Linky.propTypes = {
  activeClassName: PropTypes.string,
  to: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.array,
  ]).isRequired,
  exact: PropTypes.bool
};

export default Linky;


