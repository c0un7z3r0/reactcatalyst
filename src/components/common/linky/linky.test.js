import React from 'react';
import { test, render, customRender, renderWithRouter, fireEvent, cleanup, getByText } from '../../../../tools/testUtils.jsx';

import Linky from './linky.jsx'
import { testModeAPI } from 'react-ga';

describe("Basic Render Testing", () => {
  
  afterEach(cleanup);
  
  it("matches snapshot", () => {
    const { asFragment } = customRender(
      <Linky 
        to={"yo/momma"}
        activeClassName={"activeClassName"}
        className={"className"}
        exact={true}
      >
        {`Link Text`}
      </Linky>
    );
    expect(asFragment()).toMatchSnapshot();
  });
  
});
