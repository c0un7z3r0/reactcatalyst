import React from 'react';
import PropTypes from 'prop-types';
import styles from './button.scss';

// Bind classnames module to use our css-modules
import classNamesModule from 'classnames/bind';
let classnames = classNamesModule.bind(styles);

class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    let wrapperClasses = styles.buttonContainer;
    if (this.props.error && this.props.error.length > 0) {
      wrapperClasses += " " + 'has-error';
    }

    let iconClasses = styles.buttonIcon;
    if (this.props.iconClass) {
      iconClasses += " " + this.props.iconClass;
    }

    let buttonClassnames = classnames({
      buttonStyles : true,
      fullWidthButton : this.props.fullwidth
    });

    let button = (
      <button
        autoFocus={this.props.autofocus}
        defaultValue={this.props.defaultValue}
        disabled={this.props.disabled}
        form={this.props.form}
        formAction={this.props.form}
        formEncType={this.props.form}
        formMethod={this.props.form}
        formNoValidate={this.props.form}
        formTarget={this.props.form}
        name={this.props.name}
        value={this.props.value}
        onClick={this.props.onClick}
        className={buttonClassnames+" "+this.props.classname}
        >
        {this.props.iconClass && <span className={iconClasses}></span>}
        {this.props.children}
      </button>
    );

    let linkWrapper = "";
    if(this.props.linkUrl){
      linkWrapper = (
        <a href={this.props.linkUrl} className={styles.linkElm}>
          {button}
        </a>
      );
    }else{
      linkWrapper = (
        button
      );
    }

    return (
      <div className={wrapperClasses}>

        {this.props.label && <label htmlFor={this.props.name} className={styles.label}>{this.props.label}</label>}

        {linkWrapper}

        {this.props.error && <div className={styles.errorMessage}>{this.props.error}</div>}

      </div>
    );
  }
}

Button.propTypes = {
  autofocus: PropTypes.bool,
  children: PropTypes.string.isRequired,
  classname: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  form: PropTypes.string,
  formAction: PropTypes.string,
  formEncType: PropTypes.string,
  formMethod: PropTypes.string,
  formNoValidate: PropTypes.string,
  formTarget: PropTypes.string,
  fullwidth: PropTypes.bool,
  iconClass: PropTypes.string,
  label: PropTypes.string,
  linkUrl: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  value: PropTypes.string
};

export default Button;
