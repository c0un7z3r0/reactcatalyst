import React from 'react';
import PropTypes from 'prop-types';
import styles from './code.scss';

//USAGE
//import Code from '../../common/code/code.js';
//<Code> {this.props.children} </Code>

const Code = ({ children }) =>
  <div className={styles.codeContainer}>
    <pre className={styles.pre}>
      <code className={styles.code}>
        {children}
      </code>
    </pre>
  </div>

Code.propTypes = {
  children: PropTypes.node
}
// Code.defaultProps = {
//   className: PropTypes.string,
//   children: PropTypes.node
// }

export default Code;