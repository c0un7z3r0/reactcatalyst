import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './bugger.scss';

class Bugger extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      hasError: false,
      componentStack: "",
      message: "",
    };
  }

  componentDidCatch(error, info){
    this.setState({
      hasError:true,
      componentStack: info.componentStack,
      message: error.message,
      error: error,
      info: info
    });
  }

  // ಠ_ಠ
  // https://github.com/dysfunc/ascii-emoji

  render() {
    if(this.state.hasError){
      return (
        <div className={styles.absErrorContainer}>
          <div  className={styles.relErrorContainer}>
            <div className={styles.flashBox}>
              <p>Software Failure. Press left mouse button to continue.</p>
              <p>Guru Meditation # &nbsp;"{this.state.message}"</p>
            </div>
            <pre className={styles.errorDetails}>{this.state.componentStack}</pre>
          </div>
        </div>
      );
    } else {
      return this.props.children;
    }
  }
}

Bugger.propTypes = {
};

export default Bugger;
