import React from 'react';
import PropTypes from 'prop-types';
import styles from './textInput.scss';

class TextInput extends React.Component {
  constructor(props) {
    super(props);
  }

render() {

    let wrapperClasses = styles.inputContainer;
    if (this.props.error && this.props.error.length > 0) {
      wrapperClasses += " " + styles.denoteError;
    }

    return (
      <div className={wrapperClasses}>
        {this.props.label && <label htmlFor={this.props.name} className={styles.label}>{this.props.label}</label>}
        <input
          type="text"
          autoComplete={this.props.autocomplete}
          autoFocus={this.props.autofocus}
          defaultValue={this.props.defaultValue}
          disabled={this.props.disabled}
          form={this.props.form}
          maxLength={this.props.maxlength}
          name={this.props.name}
          pattern={this.props.pattern}
          placeholder={this.props.placeholder}
          readOnly={this.props.readonly}
          required={this.props.required}
          size={this.props.size}

          value={this.props.value}
          onChange={this.props.onChange}
          className={styles.formTextInput+" "+this.props.classname}
        />
        {this.props.error && <div className={styles.errorMessage}>{this.props.error}</div>}
      </div>
    );
  }
}

TextInput.propTypes = {
  autocomplete: PropTypes.string,
  autofocus: PropTypes.bool,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  form: PropTypes.string,
  maxlength: PropTypes.string,
  name: PropTypes.string.isRequired,
  pattern: PropTypes.string,
  placeholder: PropTypes.string,
  readonly: PropTypes.bool,
  required: PropTypes.bool,
  size: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  classname: PropTypes.string,
  error: PropTypes.string
};

export default TextInput;
