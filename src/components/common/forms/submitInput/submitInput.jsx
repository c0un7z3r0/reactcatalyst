import React from 'react';
import PropTypes from 'prop-types';
import styles from './submitInput.scss';

class SubmitInput extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    let wrapperClasses = styles.inputContainer;
    if (this.props.error && this.props.error.length > 0) {
      wrapperClasses += " " + styles.denoteError;
    }

    return (
      <div className={wrapperClasses}>
        {this.props.label && <label htmlFor={this.props.name} className={styles.label}>{this.props.label}</label>}
        <input
          type="submit"
          autoFocus={this.props.autofocus}
          disabled={this.props.disabled}
          form={this.props.form}
          formAction={this.props.form}
          formEncType={this.props.form}
          formMethod={this.props.form}
          formNoValidate={this.props.form}
          formTarget={this.props.form}
          name={this.props.name}
          value={this.props.value}
          onClick={this.props.onClick}
          className={styles.formSubmitInput+" "+this.props.classname}
        />
        {this.props.error && <div className={styles.errorMessage}>{this.props.error}</div>}
      </div>
    );
  }
}

SubmitInput.propTypes = {
  autofocus: PropTypes.bool,
  classname: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  form: PropTypes.string,
  formAction: PropTypes.string,
  formEncType: PropTypes.string,
  formMethod: PropTypes.string,
  formNoValidate: PropTypes.string,
  formTarget: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  value: PropTypes.string.isRequired
};

export default SubmitInput;
