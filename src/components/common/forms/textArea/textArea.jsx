import React from 'react';
import PropTypes from 'prop-types';
import styles from './textArea.scss';

//http://stackoverflow.com/questions/42296893/how-to-define-property-in-one-component-and-pass-to-other-component-in-reactjs
//SampleParent.defaultProps = {
//  Header: "Header from parent"
//}


//  http://stackoverflow.com/questions/23616226/insert-html-with-react-variable-statements-jsx

class TextArea extends React.Component {
  constructor(props) {
    super(props);
      if (this.props.value) {
        this.state = {
          value: this.props.value
        };
      } else {
        this.state = {
          placeholder: 'This is a controlled component '
        };
      }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.onChange(event);
  }

  render() {

    let wrapperClasses = styles.textAreaContainer;
    if (this.props.error && this.props.error.length > 0) {
      wrapperClasses += " " + styles.denoteError;
    }

    return (
      <div className={wrapperClasses}>
        <label htmlFor={this.props.name} className={styles.label}>
          {this.props.label}
        </label>
        <textarea
          autoFocus={this.props.autofocus}
          cols={this.props.cols}
          defaultValue={this.props.defaultValue}
          disabled={this.props.disabled}
          form={this.props.form}
          maxLength={this.props.maxlength}
          name={this.props.name}
          placeholder={this.props.placeholder}
          readOnly={this.props.readonly}
          required={this.props.required}
          rows={this.props.rows}
          value={this.state.value}
          wrap={this.props.wrap}
          onChange={this.handleChange}
          className={styles.textarea+' '+this.props.classname}
        />
        {this.props.error && <div className={styles.errorMessage}>{this.props.error}</div>}
      </div>
    );
  }
}

TextArea.propTypes = {
  autofocus: PropTypes.bool,
  cols: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  form: PropTypes.string,
  maxlength: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  readonly: PropTypes.bool,
  required: PropTypes.bool,
  rows: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  wrap: PropTypes.string,
  onChange: PropTypes.func,
  classname: PropTypes.string,
  error: PropTypes.string
};
export default TextArea;
