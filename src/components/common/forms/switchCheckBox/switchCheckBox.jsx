import React from 'react';
import PropTypes from 'prop-types';
import styles from './switchCheckBox.scss';

class SwitchCheckBox extends React.Component {
  constructor(props) {
    super(props);

    if(this.props.defaultChecked){
      this.state = {
        checked:  true
      };
    }else{
      this.state = {
        checked:  false
      };
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({checked: !this.state.checked});
  }

  render() {

    let wrapperClasses = styles.masterContainer;
    if (this.props.error && this.props.error.length > 0) {
      wrapperClasses += " " + 'has-error';
    }

    return (
      <div className={wrapperClasses}>
        {this.props.label && <label htmlFor={this.props.name} className={styles.label}>{this.props.label}</label>}
        <div className={styles.switchContainer}>
          <input
            type="checkbox"

            checked={this.state.checked}
            onChange={this.handleChange}

            autoFocus={this.props.autofocus}
            //defaultChecked={this.props.defaultChecked}
            defaultValue={this.props.defaultValue}
            disabled={this.props.disabled}
            form={this.props.form}
            //indeterminate={this.props.indeterminate}
            name={this.props.name}
            required={this.props.required}
            value={this.props.value}

            className={styles.switchCheckBox+" "+this.props.classname}
          />
          <div className={styles.switchGraphics}></div>
        </div>
        {this.props.error && <div className={styles.errorMessage}>{this.props.error}</div>}
      </div>
    );
  }
}

SwitchCheckBox.propTypes = {
  autofocus: PropTypes.bool,
  checked: PropTypes.bool,
  defaultChecked: PropTypes.bool,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  form: PropTypes.string,
  indeterminate: PropTypes.string,
  name: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  onClick: PropTypes.func,
  classname: PropTypes.string,
  error: PropTypes.string
};

export default SwitchCheckBox;
