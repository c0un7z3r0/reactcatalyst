import React from 'react';
import PropTypes from 'prop-types';
import styles from './selectInput.scss';

class SelectInput extends React.Component {
  constructor(props) {
    super(props);

    //if there is a defaultOption set then set the state of the default by default (obvs)
    if (this.props.defaultOption) {
      this.state = {
        value: 'defaultOptionValue'
      };
    } else {
      this.state = {
        value: this.props.options[0].value
      };
    }

    this.handleChange = this.handleChange.bind(this);
  }

  //change handler - if the user selects an option then that gets set in state
  // and thats what gets passed up to the form handler
  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.onChange(event);
  }

  render(){

    let defaultOptionValue = 'defaultOptionValue';
    let wrapperClasses = styles.dropdownContainer;
    if (this.props.error && this.props.error.length > 0) {
      wrapperClasses += " " + styles.denoteError;
    }

    return (
      <div className={wrapperClasses}>
        <label htmlFor={this.props.name} className={styles.label}>{this.props.label}</label>
        {/* Note, value is set here rather than on the option - docs: https://facebook.github.io/react/docs/forms.html */}
        <div className={styles.selectWrapper}>
          <select
            name={this.props.name}
            onChange={this.handleChange}
            className={styles.formSelectDropdown+" "+this.props.classname}
          >
            {/* If a default option is set, it will go first (like a label "select X") */}
            {this.props.defaultOption &&
            <option key="defaultOption" value={defaultOptionValue} className={styles.option}>
              {this.props.defaultOption}
            </option>
            }

            {/* Itterate through the options passed in and create a select item for each */}
            {this.props.options.map((option) => {
              return  (<option key={option.value} value={option.value} className={styles.option}>
                        {option.text}
                       </option>);
            })}
          </select>
        </div>
        {this.props.error && <div className={styles.errorMessage}>{this.props.error}</div>}
      </div>
    );
  }
}


// TODO: 'Selected' feature (see the fiat example):
// https://www.w3schools.com/html/html_form_elements.asp

SelectInput.propTypes = {
  autofocus: PropTypes.bool,
  classname: PropTypes.string,
  defaultOption: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  form: PropTypes.string,
  label: PropTypes.string.isRequired,
  multiple: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.object),
  required: PropTypes.bool,
  size: PropTypes.string,
  value: PropTypes.string
};

export default SelectInput;
