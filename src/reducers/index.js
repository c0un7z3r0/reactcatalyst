import {combineReducers} from 'redux';
// import counter from './counterReducer';
// import dependancies from './dependancyReducer';
import { routerReducer as routing } from 'react-router-redux';

// The name you chose for your reducer is what is reflected in your components. ie
//    courses = state.courses
//    rhubarb = state.rhubarb
// See mapStateToProps in coursesPage.js for example.
  // counter,
  // dependancies,
const rootReducer = combineReducers({

  routing
});

export default rootReducer;
