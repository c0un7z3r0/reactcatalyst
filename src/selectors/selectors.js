// Transforms the author data into a format to be consumed by the presentational component.

// Left in place as an example of a jest test.
export function authorsFormattedForDropdown(authors) {
  return authors.map(author => {
    return {
      value: author.id,
      text: author.firstName + ' ' + author.lastName
    };
  });
}
