import React from 'react';
import { render } from 'react-dom';
import { hot } from 'react-hot-loader'

import {Provider} from 'react-redux';
import configureStore from './store/configureStore';

import MainLayout from './components/Layouts/mainLayout/mainLayout.jsx';

import {
  Router,
  Route,
  Link
} from 'react-router-dom';

import {createBrowserHistory} from 'history'
const history = createBrowserHistory()


// GOOGLE ANALYTICS OPTIONAL:
// 
// import withTracker from './withTracker.js';
// i. Uncomment the Import above. 
// ii. Add your google analytics tracking id code to 'withTracker.js' 
// iii. replace the main route below with this: <Route component={withTracker(MainLayout)}/>

// SCSS/CSS - EXTERNAL MODULE IMPORTS
//
// No-name SCSS/CSS imports from the static/styles folder will
// be bundled into the compiled stylesheet as is - there are no
// BEM class transformation applied as there are with named
// CSS Modules as there are when using: import styles from './someComponent.scss')
import './static/styles/styles.scss';
import './static/styles/grid.scss';
import './static/styles/base-theme/base.scss';
import './static/styles/typography.scss';
import './static/styles/base-theme/scrollbars.scss';

//const store = createStore(configureStore);
const store = configureStore();

//load courses on init.
//store.dispatch(loadDependancies());

class App extends React.Component {
  render() {
    return(
      <Provider store={store}>
        <Router history={history}>
          <Route component={MainLayout}/>
        </Router>
      </Provider>
    );
  }
}
//export default App;
export default hot(module)(App)
