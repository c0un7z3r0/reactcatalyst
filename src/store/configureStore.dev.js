import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from '../reducers';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

// Because: https://github.com/reactjs/react-redux/releases/tag/v2.0.0
//
// export default function configureStore(initialState) {
//   return createStore(
//     rootReducer,
//     initialState,
//     compose(
//       applyMiddleware(thunk, reduxImmutableStateInvariant()),
//       window.devToolsExtension ? window.devToolsExtension() : f => f
//     )
//   );
// }

export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(
      applyMiddleware(thunk, reduxImmutableStateInvariant())
    )
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
