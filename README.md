# React Catalyst
Yet another react redux starter kit.    

## Get Started
1. **Install Node Packages.** - `npm install`
2. **Run the app.** - `npm start -s`
This will run the automated build process, start up a webserver, and open the application in your default browser. When doing development with this kit, this command will continue watching files all your files. Every time you hit save the code is rebuilt, linting runs, and tests run automatically. Note: The -s flag is optional. It enables silent mode which suppresses unnecessary messages during the build.
3. **Supports both [React developer tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) and [Redux Dev Tools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)** in Chrome.
4. **If hot reloading is failing [disable safe write](http://webpack.github.io/docs/webpack-dev-server.html#working-with-editors-ides-supporting-safe-write)** to assure hot reloading works properly.

##Scripts
** The important bits **

`npm run start`
Runs an NPM package check to ensure there are no security concerns or bugs with the dependancies, runs Mocha tests, starts up hot-reloading dev webserver and watches for any changes and lints your code as you work.

`npm run build` This is your production build. It cleans the 'dist' directory, runs your tests, starts linting and builds the html file that serves your bundle then generates your bundle and fires up a dist server, this also support mockAPIs where required.

`npm run share` uses localtunnel to enable you to quickly share what is running on your local machine as you would any other web page. It provides you with a URL you can send to people to show what is running on Localhost. This is perfect for quickly and easily showing work to a collegue or client without committing and deploying first.

`npm run deploy` deploys the contents of the /dist/ folder, via FTP, to the server specified in the envConfig.js file.

** The stuff you'll probably never need to use **

`npm run lint` lints your js. Runs parallel to the hot-reloading so you shouldnt need to run this seperately.

`npm security-check` checks all your dependancies for known security issues with all used modules.

`npm run test` runs mocha tests. Also runs parallel to the hot-reloading so you shouldnt need to run this seperately.


##Dependencies

###Production Dependencies
| **Dependency** | **Use** | **Link** |
|----------|-------|------|
|babel-polyfill | Polyfill for Babel features that cannot be transpiled | [GO](https://www.npmjs.com/package/babel-polyfill)
|react|React library | [GO](https://www.npmjs.com/package/react)
|react-dom|React library for DOM rendering | [GO](https://www.npmjs.com/package/react-dom)
|react-redux|Redux library for connecting React components to Redux | [GO](https://www.npmjs.com/package/react-redux)
|react-router|React library for routing | [GO](https://www.npmjs.com/package/react-router)
|react-router-redux|Keep React Router in sync with Redux application state| [GO](https://www.npmjs.com/package/react-router-redux)
|redux|Library for unidirectional data flows | [GO](https://www.npmjs.com/package/redux)
|redux-thunk|Async redux library| [GO](https://www.npmjs.com/package/redux-thunk)

###Development Dependencies
| **Dependency** | **Use** | **Link** |
|----------|-------|------|
|babel-cli|Babel Command line interface | [GO](https://www.npmjs.com/package/babel-cli)
|babel-core|Babel Core for transpiling the new JavaScript to old | [GO](https://www.npmjs.com/package/babel-core)
|babel-loader|Adds Babel support to Webpack | [GO](https://www.npmjs.com/package/babel-loader)
|babel-plugin-react-display-name| Add displayName to React.createClass calls | [GO](https://www.npmjs.com/package/babel-plugin-react-display-name)
|babel-preset-es2015|Babel preset for ES2015| [GO](https://www.npmjs.com/package/babel-preset-es2015)
|babel-preset-react| Add JSX support to Babel | [GO](https://www.npmjs.com/package/babel-preset-react)
|babel-preset-react-hmre|Hot reloading preset for Babel| [GO](https://www.npmjs.com/package/babel-preset-react-hmre)
|babel-register|Register Babel to transpile our Mocha tests| [GO](https://www.npmjs.com/package/babel-register)
|colors|Adds color support to terminal | [GO](https://www.npmjs.com/package/colors)
|compression|Add gzip support to Express| [GO](https://www.npmjs.com/package/compression)
|CopyWebpackPlugin|Copy static files to dist folder, in our case, the favicons| [GO](https://github.com/kevlened/copy-webpack-plugin)
|cross-env|Cross-environment friendly way to handle environment variables| [GO](https://www.npmjs.com/package/cross-env)
|css-loader|Add CSS support to Webpack| [GO](https://www.npmjs.com/package/)
|enzyme|Simplified JavaScript Testing utilities for React| [GO](https://www.npmjs.com/package/enzyme)
|eslint|Lints JavaScript | [GO](https://www.npmjs.com/package/eslint)
|eslint-plugin-import|Advanced linting of ES6 imports| [GO](https://www.npmjs.com/package/eslint-plugin-import)
|eslint-plugin-react|Adds additional React-related rules to ESLint| [GO](https://www.npmjs.com/package/eslint-plugin-react)
|eslint-watch|Add watch functionality to ESLint | [GO](https://www.npmjs.com/package/eslint-watch)
|eventsource-polyfill|Polyfill to support hot reloading in IE| [GO](https://www.npmjs.com/package/eventsource-polyfill)
|expect|Assertion library for use with Mocha| [GO](https://www.npmjs.com/package/expect)
|express|Serves development and production builds| [GO](https://www.npmjs.com/package/express)
|extract-text-webpack-plugin| Extracts CSS into separate file for production build |  [GO](https://www.npmjs.com/package/extract-text-webpack-plugin)
|file-loader| Adds file loading support to Webpack | [GO](https://www.npmjs.com/package/file-loader)
|jsdom|In-memory DOM for testing| [GO](https://www.npmjs.com/package/jsdom)
|json-loader|Enables webpack <2 to handle JSON files | [GO](https://github.com/webpack-contrib/json-loader)
|mocha| JavaScript testing library | [GO](https://www.npmjs.com/package/mocha)
|nock| Mock HTTP requests for testing | [GO](https://www.npmjs.com/package/nock)
|npm-run-all| Display results of multiple commands on single command line | [GO](https://www.npmjs.com/package/npm-run-all)
|nsp| Checks all packages used for known issues when running dev mode | [GO](https://www.npmjs.com/package/nsp)
|open|Open app in default browser| [GO](https://www.npmjs.com/package/open)
|postcss-loader | transforms css styles with a number of plugins - used in this case for autoprefixer and css modules | [GO](https://www.npmjs.com/package/postcss-loader)
|react-addons-test-utils| Adds React TestUtils | [GO](https://www.npmjs.com/package/react-addons-test-utils)
|redux-immutable-state-invariant|Warn when Redux state is mutated| [GO](https://www.npmjs.com/package/redux-immutable-state-invariant)
|redux-mock-store|Mock Redux store for testing| [GO](https://www.npmjs.com/package/redux-mock-store)
|rimraf|Delete files | [GO](https://www.npmjs.com/package/rimraf)
|sass-loader| Add SASS support to Webpack | [GO](https://www.npmjs.com/package/sass-loader)
|style-loader| Add Style support to Webpack | [GO](https://www.npmjs.com/package/style-loader)
|url-loader| Add url loading support to Webpack | [GO](https://www.npmjs.com/package/url-loader)
|webpack| Bundler with plugin system and integrated development server | [GO](https://www.npmjs.com/package/webpack)
|webpack-dev-middleware| Adds middleware support to webpack | [GO](https://www.npmjs.com/package/webpack-dev-middleware)
|webpack-hot-middleware| Adds hot reloading to webpack | [GO](https://www.npmjs.com/package/webpack-hot-middleware)

##TODO
** Things to document: **

-How vendor.js works: Jist - in dev mode it just gets bundled into one bundle.js file by webpack dev server. In production, it has 2 files: vendor.js and bundle.js. Vendor.js has the common dependancies for user caching purposes.

-How to use the favicon feature: Jist - use this generator (http://www.favicon-generator.org/) and lump the files in the static/images/favicon folder.

-Testing stuff - thats gone in one ear and out the other unfortunately. See tools/testSetup.js and if needs be, go back to pluralsight.

-CSS: in index.js the styles.css import is essential, reset.css recommended and the rest optional.

-CSS: explain how the imports work ('static/lib.css' vs 'componentFolder/component.css' ie. CSSmodules)

-IMAGE USAGE: when to use js import and when to use static folder: ask yourself: is this image going to always and only be used in this component context. If the answer is yes to both then it belongs in the component, if its no to either it belongs in the static folder.
