import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import {version, author, dependencies, devDependencies } from './package.json'

const plugins = [
  new HtmlWebpackPlugin({
    template: 'tools/serverIndex.html',
    inject: false,
    // Properties you define here are available in src/index.html
    // using htmlWebpackPlugin.options.varName
    showHeaderMeta:false
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new CopyWebpackPlugin([
    { from: 'src/static/images/favicon', to: 'images/favicon' }
  ]),
  // USE THIS TO SET BUNDLE VARIABLES. {VERSION} is available throughout the app in the example below:
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('development')
    },
    CATALYST: {
      VERSION: JSON.stringify(version),
      AUTHOR: JSON.stringify(author),
      DEPENDENCIES: JSON.stringify(dependencies),
      DEVDEPENDENCIES: JSON.stringify(devDependencies),
    }
  }),
];

export default {
  mode: 'development',
  resolve: {
    extensions: ['.js', '.jsx'],
    // https://github.com/gaearon/react-hot-loader/issues/1227
    alias: { 'react-dom': '@hot-loader/react-dom'  }
  },
  //debug: true,
  devtool: 'inline-source-map',
  entry:[
    'webpack-hot-middleware/client?reload=true',
    './src/index.js'
  ],
  target: 'web',
  output: {
    path: path.resolve(__dirname + '/dist'), // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
    // EXPOSE EXPORTED FUNCTIONS in index.js ( eg. export function kungfoo(){...} )
    // Uncomment the example below and you would use: Shaolin.kungfoo()
    // note: Untested in dev environment.
    //
    // libraryTarget: 'var',
    // library: 'Shaolin'
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'src'),
    hot:true
  },
  plugins,
  module: {
    rules: [
        // JS
        {
          test: /\.(js||jsx)$/, 
          include: path.resolve(__dirname, 'src'), 
          loader: ['babel-loader']
        },

        // CSS These 2 loaders for css are deliberate, it enables cssmodules to be inlined in DEV and any libs from
        // 'node_modules' or 'src/static' to be merged into the compiled stylesheet untransformed.
        {
          test: /(\.css)$/,
          use:[
            'style-loader',
            'css-loader'
          ],
          include:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
        },
        {
          test: /(\.css)$/,
          use:[
            {
              loader: 'style-loader',
              options: {
                sourceMap: true
              }
            },{
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                localIdentName: '[name]__[local]___[hash:base64:5]',
                modules: true,
                sourceMap: true
              }
            },{
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: function () {
                  return [
                    require('autoprefixer')
                  ];
                }
              }
            }
          ],
          exclude:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
        },
        // SCSS These 2 loaders for css are deliberate, it enables cssmodules to be inlined in DEV and any libs from
        // 'node_modules' or 'src/static' to be merged into the compiled stylesheet.
        {test: /(\.scss)$/,
          use:[
            {
              loader: 'style-loader',
              options: {
                sourceMap: true
              }
            },{
              loader: 'css-loader',
              options: {
                //localIdentName: '[hash:base64]-[name]-[local]',
                //modules: true,
                sourceMap: true
              }
            },{
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: function () {
                  return [
                    require('autoprefixer')
                  ];
                }
              }
            },{
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ],
          include:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
        },
        {test: /(\.scss)$/,
          use:[{
              loader: 'style-loader',
              options: {
                sourceMap: true
              }
            },{
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                localIdentName: '[name]__[local]___[hash:base64:5]',
                modules: true,
                sourceMap: true
              }
            },{
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: function () {
                  return [
                    require('autoprefixer')
                  ];
                }
              }
            },{
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ],
          exclude:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
        },
        // FONTS
        {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader', options:{name: '[name].[ext]'}},
        {test: /\.(woff|woff2)$/, loader: 'file-loader', options:{name:'[name].[ext]'}},
        {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader', options:{name: '[name].[ext]'}},
        {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader', options:{name: '[name].[ext]'}},
        // IMAGES
        // image files in src are handled except those in static which will be manually moved with npm script
        // this is to enable modular components to take their required assets with them in their folder.
        // Page-specific images will be loaded from the images folder when required (rather than be bundled in js)
        {
          test: /\.(jpg|png|gif)$/,
          use:[
            {
              loader: 'file-loader',
              options:{
                name:'images/[name].[ext]',
                outputPath:'static/images/'
              }
            }
          ],
          exclude: path.resolve(__dirname, 'src/static/images/*')
        },
        //VIDEO FILES:
        {
          test: /\.mp4$/,
          loader: 'url-loader?limit=10000&mimetype=video/mp4'
        },
        {
          test: /\.webm$/,
          loader: 'url-loader?limit=10000'
        },
        {
          test: /\.ogg$/,
          loader: 'url-loader?limit=10000'
        }
    ]
  }
};
