import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import {BundleAnalyzerPlugin} from 'webpack-bundle-analyzer';
import {version, author, dependencies, devDependencies } from './package.json'

const plugins = [
  
  // Create HTML file that includes reference to bundled JS.
  new HtmlWebpackPlugin({
    template: 'src/static/index.html',
    inject: true,
    minify:{
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true
    },
    // Properties you define here are available in index.html
    // using htmlWebpackPlugin.options.varName
    showHeaderMeta:false
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    },
    CATALYST: {
      VERSION: JSON.stringify(version),
      AUTHOR: JSON.stringify(author),
      DEPENDENCIES: JSON.stringify(dependencies),
      DEVDEPENDENCIES: JSON.stringify(devDependencies),
    }
  }),
  new ExtractTextPlugin({filename: 'styles/styles.css', allChunks: true}),
  new webpack.optimize.AggressiveMergingPlugin(),

  new CopyWebpackPlugin([
    // Copy Favicons
    { from: 'src/static/images/favicon', to: 'images/favicon' }
  ]),

  new BundleAnalyzerPlugin({
    analyzerMode: 'static'
  }),

  new webpack.LoaderOptionsPlugin({
    options: {
      postcss: [
        autoprefixer()
      ]
    }
  })
];

export default {
  // WEBPACK 4 ADDITIONS
  mode: 'production',
  optimization:{
    minimize:true,
    splitChunks: {
      chunks: "async",
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "all"
        },
        main: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  
  resolve: {
    extensions: ['.js', '.jsx']
  },

  entry: {
    vendor: path.resolve(__dirname, 'src/vendor'),
    main: path.resolve(__dirname, 'src/index')
  },
  //debug: true,
  //noInfo: false,
  target: 'web',
  output: {
    path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: '[name].js',
    // EXPOSE EXPORTED FUNCTIONS in index.js ( eg. export function kungfoo(){...} )
    // Uncomment the example below and you would use: Shaolin.kungfoo()
    //
    // libraryTarget: 'var',
    // library: 'Shaolin'
  },
  devServer: {
    contentBase: './dist'
  },
  plugins,
  module: {
    rules:[

      {test: /\.(js||jsx)$/, include: path.resolve(__dirname, 'src'), loaders: ['babel-loader']},

      // CSS, SCSS & CSS Modules (note the include/exclude paths - this enables libraries 
      // to be imported into your app without the CSS modules classname BEM transforms)
      {test: /(\.css)$/,
        use: ExtractTextPlugin.extract({
          fallback:[
            {
              loader: 'style-loader'
            }
          ],
          use:[
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1
              }
            },{
              loader: 'postcss-loader'
            }
          ]
        }),
        include:  [path.join(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
      },

      {test: /(\.css)$/,
        use: ExtractTextPlugin.extract({
          fallback:[
            {
              loader: 'style-loader'
            }
          ],
          use:[
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                localIdentName: '[name]__[local]___[hash:base64:5]',
                modules: true
              }
            },{
              loader: 'postcss-loader'
            }
          ]
        }),
        exclude:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
      },
      {test: /(\.scss)$/,
        use: ExtractTextPlugin.extract({
          fallback:[
            {
              loader: 'style-loader'
            }
          ],

          use:[
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1
              }
            },{
              loader: 'postcss-loader'
            },{
              loader: 'sass-loader'
            }
          ]
        }),
        include:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
      },
      {test: /(\.scss)$/,
        use: ExtractTextPlugin.extract({
          fallback:[
            {
              loader: 'style-loader'
            }
          ],
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                localIdentName: '[name]__[local]___[hash:base64:5]',
                modules: true
              }
            }, {
              loader: 'postcss-loader'
            }, {
              loader: 'sass-loader'
            }
          ]
        }),
        exclude:  [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/static') ]
      },

      //FONTS
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader', options:{name: 'fonts/[name].[ext]'}},
      {test: /\.(woff|woff2)$/, loader: 'file-loader', options:{name:'fonts/[name].[ext]'}},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader', options:{name: 'fonts/[name].[ext]'}},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader', options:{name: 'fonts/[name].[ext]'}},

      //IMAGES in src are handled by webpack, except those in static which will be manually moved with an npm
      // script. This is to enable modular components to take their required assets with them in their folder
      // and page-specific images to be loaded from the images folder when required (rather than be bundled)
      {
        test: /\.(jpg|png|gif)$/,
        use:[
          {
            loader: 'file-loader',
            options:{
              name:'images/[name].[ext]'
            }
          }
        ],
        exclude: path.resolve(__dirname, 'src/static/images/*')
      },
      //VIDEO FILES:
      {
        test: /\.mp4$/,
        loader: 'url-loader?limit=10000&mimetype=video/mp4'
      },
      {
        test: /\.webm$/,
        loader: 'url-loader?limit=10000'
      },
      {
        test: /\.ogg$/,
        loader: 'url-loader?limit=10000'
      }
    ]
  }
};
